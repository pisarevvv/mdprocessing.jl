Base.IteratorEltype(::Type{MDState}) = Base.HasEltype()

Base.eltype(::Type{MDState}) = MDParticle

Base.IteratorSize(::Type{MDState}) = Base.HasLength()

Base.@propagate_inbounds function Base.iterate(state::MDState, idx::Int=1)
    idx in eachindex(state.coord) || return nothing
    return MDParticle(state, idx), idx+1
end

Base.length(state::MDState) = length(state.coord)

"""
    filter(predicate, system::MDState)

Return a copy of `system` which contains only the particles satisfying the `predicate`.
"""
function Base.filter(fn, system::MDState)
    keep = [fn(part) for part in system]
    filtered = MDState(box_vectors=boxvectors(system), pbc=system.pbc, origin=system.origin)
    filtered.timestep = timestep(system)
    filtered._hasfields = system._hasfields
    map((:id, :type, :mol, :_image, :coord, :vel, :_mass, :_element)) do attr
        @views append!(getfield(filtered, attr), getfield(system, attr)[keep])
    end
    for (k, v) in system._scalars
       @views addproperty!(filtered; name=k, data=v[keep], allow_sharing=true)
    end
    for (k, v) in system._vectors
        @views addproperty!(filtered; name=k, data=v[keep], allow_sharing=true)
    end
    filtered.nreal = length(filtered.coord)
    return filtered
end
