"""
Provide intitial values for reducing functions.
"""
struct _ReduceEmpty end

_reduce(::typeof(+), ::_ReduceEmpty, x) = x
_reduce(::typeof(*), ::_ReduceEmpty, x) = x
_reduce(::typeof(min), ::_ReduceEmpty, x) = x
_reduce(::typeof(max), ::_ReduceEmpty, x) = x
_reduce(::typeof(|), ::_ReduceEmpty, x) = x
_reduce(::typeof(&), ::_ReduceEmpty, x) = x
_reduce(::typeof(xor), ::_ReduceEmpty, x) = x
_reduce(::typeof(string), ::_ReduceEmpty, x) = string(x)
_reduce(::typeof(vcat), ::_ReduceEmpty, x::AbstractArray) = collect(x)
_reduce(::typeof(vcat), ::_ReduceEmpty, x) = [x]
_reduce(::typeof(hcat), ::_ReduceEmpty, x::AbstractArray) = collect(x)
_reduce(::typeof(hcat), ::_ReduceEmpty, x) = [x]
_reduce(op, ::_ReduceEmpty, x) = Base.mapreduce_empty(identity, op, Union{})

"""
    neighbor_pairs(system::MDState, rcut::Real)
    neighbor_pairs(system::MDState; rcut::Real)

Return an iterable objects of all pairs in `system` separated by the distance less than
    `rcut`, accounting for periodic boundaries.
"""
function neighbor_pairs(system::MDState; rcut::Real)
    return NeighborPairs(system, rcut, nothing)
end

function neighbor_pairs(system::MDState, rcut::Real)
    return NeighborPairs(system, rcut, nothing)
end

struct NeighborPairs{B,C,I<:CartesianIndices,F}
    coord::VecVec{Float64}
    boxsize::B
    cell_list::C
    cartinds::I
    rsq::Float64
    pbc::NTuple{3,Bool}
    predicate::F
end

Base.IteratorSize(::Type{<:NeighborPairs}) = Base.SizeUnknown()

Base.IteratorEltype(::Type{<:NeighborPairs}) = Base.HasEltype()

Base.eltype(::Type{<:NeighborPairs}) = ParticlePair

function NeighborPairs(system::MDState, rcut::Real, predicate=nothing)
    # Guarantee that all `r`s such that `r^2 < rsq` are less than `rcut`
    rc = float(rcut)
    rsq = nextfloat((prevfloat(rc))^2)
    cell_list = CellList(system, rc)
    return NeighborPairs(
        system.coord,
        diag(boxvectors(system)),
        cell_list,
        CartesianIndices(cell_list),
        rsq,
        system.pbc,
        predicate,
    )
end

Base.@inline function Base.iterate(pairs::NeighborPairs)
    cartinds_next = iterate(pairs.cartinds)
    while cartinds_next !== nothing
        idx = cartinds_next[1]
        cartinds_iterstate = cartinds_next[2]
        @inbounds cell_particles = pairs.cell_list[idx]
        if !isempty(cell_particles)
            neigh_i = neigh_atoms(pairs.cell_list, idx)
            neigh_i_next = iterate(neigh_i)
            while neigh_i_next !== nothing
                ind1 = neigh_i_next[1]
                neigh_iterstate = neigh_i_next[2]
                @inbounds r1 = pairs.coord[ind1]
                cell_next = iterate(cell_particles)
                while cell_next !== nothing
                    ind2 = cell_next[1]
                    cell_iterstate = cell_next[2]
                    ind2 < ind1 || break
                    @inbounds r2 = pairs.coord[ind2]
                    r12 = pbcvector(r1, r2, pairs.boxsize, pairs.pbc)
                    sqdist = mag2(r12)
                    if sqdist < pairs.rsq
                        ppair = ParticlePair(ind1, ind2, r12, sqdist)
                        if pairs.predicate === nothing || pairs.predicate(ppair)
                            return (
                                ppair,
                                (
                                    cartinds_iterstate,
                                    neigh_i,
                                    neigh_iterstate,
                                    ind1,
                                    r1,
                                    cell_particles,
                                    cell_iterstate,
                                ),
                            )
                        end
                    end
                    cell_next = iterate(cell_particles, cell_iterstate)
                end
                neigh_i_next = iterate(neigh_i, neigh_iterstate)
            end
        end
        cartinds_next = iterate(pairs.cartinds, cartinds_iterstate)
    end
    return nothing
end

Base.@inline function Base.iterate(
    pairs::NeighborPairs,
    state
)
    local cartinds_next
    (
        cartinds_iterstate,
        neigh_i,
        neigh_iterstate,
        ind1,
        r1,
        cell_particles,
        cell_iterstate,
    ) = state

    @goto iter_continue
    while cartinds_next !== nothing
        idx = cartinds_next[1]
        cartinds_iterstate = cartinds_next[2]
        @inbounds cell_particles = pairs.cell_list[idx]
        if !isempty(cell_particles)
            neigh_i = neigh_atoms(pairs.cell_list, idx)
            neigh_i_next = iterate(neigh_i)
            while neigh_i_next !== nothing
                ind1 = neigh_i_next[1]
                neigh_iterstate = neigh_i_next[2]
                @inbounds r1 = pairs.coord[ind1]
                cell_next = iterate(cell_particles)
                while cell_next !== nothing
                    ind2 = cell_next[1]
                    cell_iterstate = cell_next[2]
                    ind2 < ind1 || break
                    @inbounds r2 = pairs.coord[ind2]
                    r12 = pbcvector(r1, r2, pairs.boxsize, pairs.pbc)
                    sqdist = mag2(r12)
                    if sqdist < pairs.rsq
                        ppair = ParticlePair(ind1, ind2, r12, sqdist)
                        if pairs.predicate === nothing || pairs.predicate(ppair)
                            return (
                                ppair,
                                (
                                    cartinds_iterstate,
                                    neigh_i,
                                    neigh_iterstate,
                                    ind1,
                                    r1,
                                    cell_particles,
                                    cell_iterstate,
                                ),
                            )
                        end
                    end
                    @label iter_continue
                    cell_next = iterate(cell_particles, cell_iterstate)
                end
                neigh_i_next = iterate(neigh_i, neigh_iterstate)
            end
        end
        cartinds_next = iterate(pairs.cartinds, cartinds_iterstate)
    end
    return nothing
end

function Base.foreach(fn, pairs::NeighborPairs)
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    ppair = ParticlePair(i, j, r12, sqdist)
                    if pairs.predicate === nothing || pairs.predicate(ppair)
                        fn(ppair)
                    end
                end
            end
        end
    end
    return nothing
end

function Base.mapfoldl(fn, op, pairs::NeighborPairs; init=_ReduceEmpty())
    return _mapfoldl(fn, op, pairs, init)
end

function _mapfoldl(fn, op, pairs::NeighborPairs, init::_ReduceEmpty)
    local result
    started = false
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    ppair = ParticlePair(i, j, r12, sqdist)
                    if pairs.predicate === nothing || pairs.predicate(ppair)
                        if !started
                            result = _reduce(op, init, fn(ppair))
                            started = true
                        else
                            result = op(result, fn(ppair))
                        end
                    end
                end
            end
        end
    end
    if !started
        result = _mapreduce_empty(fn, op, init)
    end
    return result
end

function _mapfoldl(fn, op, pairs::NeighborPairs, init)
    local result
    started = false
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    ppair = ParticlePair(i, j, r12, sqdist)
                    if pairs.predicate === nothing || pairs.predicate(ppair)
                        if !started
                            result = op(init, fn(ppair))
                            started = true
                        else
                            result = op(result, fn(ppair))
                        end
                    end
                end
            end
        end
    end
    if !started
        result = _mapreduce_empty(fn, op, init)
    end
    return result
end

_mapreduce_empty(fn, op, ::_ReduceEmpty) = Base.mapreduce_empty(fn, op, Union{})
_mapreduce_empty(_, _, init) = init

function Base.in(pair::ParticlePair, neigh_pairs::NeighborPairs)
    coords = neigh_pairs.coord
    i, j = atom_indices(pair)
    r1, r2 = coords[i], coords[j]
    if pbcvector(r1, r2, neigh_pairs.boxsize, neigh_pairs.pbc) == pbcvector(pair)
        if neigh_pairs.predicate === nothing || neigh_pairs.predicate(pair)
            return pbcdist2(pair) < neigh_pairs.rsq
        else
            return false
        end
    else
        return false
    end
end

Base.in(_, ::NeighborPairs) = false

function Base.filter(
    predicate, neigh_pairs::NeighborPairs{B,C,I,Nothing},
) where {B,C,I<:CartesianIndices}
    return NeighborPairs(
        neigh_pairs.coord,
        neigh_pairs.boxsize,
        neigh_pairs.cell_list,
        neigh_pairs.cartinds,
        neigh_pairs.rsq,
        neigh_pairs.pbc,
        predicate,
    )
end

function Base.filter(predicate, neigh_pairs::NeighborPairs)
    function add_predicate(ppair)
        return neigh_pairs.predicate(ppair) && predicate(ppair)
    end
    return NeighborPairs(
        neigh_pairs.coord,
        neigh_pairs.boxsize,
        neigh_pairs.cell_list,
        neigh_pairs.cartinds,
        neigh_pairs.rsq,
        neigh_pairs.pbc,
        add_predicate,
    )
end
