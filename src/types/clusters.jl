"""
    Cluster

Structure holding atom indices of a cluster in an `MDState`. Iteration over a `Cluster`
    produces `MDParticle`.

See also: [`atom_indices(::Cluster)`](@ref), [`parent_state(::Cluster)`](@ref)
"""
struct Cluster
    id::Int
    atom_indices::IndexList{Int}
    state::MDState
end

"""
    Clusters

Structure holding information about clusters of atoms in an `MDState`. Clusters are
    non-intersecting sets of particles, and all particles within a cluster are
    interconnected by some neighboring criterion. Particles not connected to any other
    would form a single-particle cluster.

See also: [`clusters`](@ref), [`add_clusters!`](@ref), [`parent_state(::Clusters)`](@ref)
"""
struct Clusters<:AbstractVector{Cluster}
    id::Vector{Int}
    list::IndexListArray{1,Int}
    state::MDState
end

function Clusters(list::IndexListArray{1,Int}, state::MDState)
    cluster_id = similar(1:natoms(state))
    for (iclust, inner_list) in enumerate(list)
        for i in inner_list
            cluster_id[i] = iclust
        end
    end
    return Clusters(cluster_id, list, state)
end

"""
    clusters(system::MDState; bond_distance::Real, sort::Bool=true)

Return an array-like object for atom clusters in `system`. 'bond_distance' is the largest
    distance between two particles in each cluster. If `sort === true`, then the clusters
    are sorted by size from largest to smallest.

See also: [`add_clusters!`](@ref)
"""
Base.@propagate_inbounds function clusters(
    system::MDState,
    ;
    bond_distance::Real,
    sort::Bool=true,
)
    rcut = bond_distance
    if (rcut < 0)
        throw(DomainError(rcut, "Negative `bond_distance` is not allowed."))
    end
    ds = DisjointSets(natoms(system), neighbor_pairs(system, rcut))
    atom_roots = root_nodes(ds)

    root_ids = [k for (k, v) in pairs(atom_roots) if k == v]
    n_clusters = ncomponents(ds)
    n_atoms = natoms(system)
    list = IndexListArray(; dims=(n_clusters,), nitems=n_atoms)

    for k in reverse(eachindex(atom_roots))
        root_id = atom_roots[k]
        iroot = searchsortedfirst(root_ids, root_id)
        push!(list[begin+iroot-1], k)
    end

    output = Clusters(list, system)
    if sort
        sort_clusters!(output)
    end
    return output
end

function sort_clusters!(data::Clusters)
    cluster_list = data.list
    n_clusters = length(cluster_list)

    loffset = lengths_offset(cluster_list)
    cluster_sizes = @view cluster_list.data[loffset + 1:loffset + n_clusters]

    p = sortperm(cluster_sizes; rev=true)
    permuteditems = similar(p)

    hoffset = heads_offset(cluster_list)
    heads_view = @view cluster_list.data[hoffset + 1:hoffset + n_clusters]
    permuteditems .= @view heads_view[p]
    heads_view .= permuteditems

    permuteditems .= @view cluster_sizes[p]
    cluster_sizes .= permuteditems

    cluster_id = data.id
    for (icluster, atom_list) in enumerate(cluster_list)
        for iatom in atom_list
            cluster_id[iatom] = icluster
        end
    end
    return data
end

"""
    clusters(system::MDState, index_pairs; sort::Bool=true)

Return an array-like object for atom clusters in `system`. `index_pairs` is an iterable
    object with pairs of atom indices or `ParticlePair` objects. If `sort === true`, then
    the clusters are sorted by size from largest to smallest.
"""
function clusters(system::MDState, ind_pairs; sort::Bool=true)

    ds = DisjointSets(natoms(system), ind_pairs)

    atom_roots = root_nodes(ds)

    root_ids = [k for (k, v) in pairs(atom_roots) if k == v]
    n_clusters = ncomponents(ds)
    n_atoms = natoms(system)
    list = IndexListArray(; dims=(n_clusters,), nitems=n_atoms)

    for k in reverse(eachindex(atom_roots))
        root_id = atom_roots[k]
        iroot = searchsortedfirst(root_ids, root_id)
        push!(list[begin+iroot-1], k)
    end

    output = Clusters(list, system)

    if sort
        sort_clusters!(output)
    end

    return output
end
