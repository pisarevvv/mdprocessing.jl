const VecVec{T} = Vector{SVector{3,T}}

@enum CellType ORTHO_CELL TRICLINIC_CELL

mutable struct MDState
    timestep::Int
    box_type::CellType
    box_vectors::SMatrix{3,3,Float64,9}
    origin::SVector{3,Float64}
    pbc::NTuple{3,Bool}
    _hasfields::UInt16 #from the least-signif. bit: images, vx, vy, vz, mass, element
    id::Vector{Int}
    type::Vector{Int}
    mol::Vector{Int}
    _image::VecVec{Int16}
    coord::VecVec{Float64}
    vel::VecVec{Float64}
    _mass::Vector{Float64}
    _element::Vector{String}
    _scalars::Dict{Symbol, Vector{Float64}}
    _vectors::Dict{Symbol, VecVec{Float64}}
    nreal::Int
end

"""
    MDState

The data structure to store a state of MD simulation.

    MDState(
        ;
        box_vectors=[
            1 0 0
            0 1 0
            0 0 1
        ],
        box_type::Symbol,
        origin=(0, 0, 0),
        pbc=(true, true, true),
    )

Create a new MDState without particle information.
# Keywords
- `box_vectors`: cell vectors of the simulation box as columns in a form of a matrix
- `box_type`: type of simulation box cell. Must be `:orthogonal` or `:triclinic`, otherwise
    an error is thrown. Default: `:orthogonal` if `box_vectors` is diagonal matrix,
    `:triclinic` otherwise
- `origin`: the coordinate origin of the simulation cell
- `pbc`: periodic boundaries along three box vectors (`true` / `false`)

# Throws
- `ArgumentError` is thrown if `box_type == :orthogonal` and `box_vectors` is not a
    diagonal matrix
"""
function MDState(
    ;
    box_vectors=SMatrix{3,3,Float64}(I),
    box_type::Symbol=isdiag(box_vectors) ? :orthogonal : :triclinic,
    origin=(0, 0, 0),
    pbc=(true, true, true),
)
    if !in(box_type, (:orthogonal, :triclinic))
        throw(ArgumentError("`box_type` must be `:orthogonal` or `:triclinic`"))
    end
    if box_type === :orthogonal && !isdiag(box_vectors)
        throw(ArgumentError("Orthogonal box vectors must form a diagonal matrix"))
    end
    return MDState(
        0,
        box_type === :orthogonal ? ORTHO_CELL : TRICLINIC_CELL,
        box_vectors,
        origin,
        pbc,
        0x0000,
        Int[],
        Int[],
        Int[],
        VecVec{Int16}(),
        VecVec{Float64}(),
        VecVec{Float64}(),
        Float64[],
        String[],
        Dict{Symbol, Vector{Float64}}(),
        Dict{Symbol, VecVec{Float64}}(),
        0
    )
end

const HASIMAGES_MASK = UInt16(0b1)
const HASVX_MASK = UInt16(0b10)
const HASVY_MASK = UInt16(0b100)
const HASVZ_MASK = UInt16(0b1000)
const HASMASS_MASK = UInt16(0b10000)
const HASELEMENT_MASK = UInt16(0b100000)
const STATE_FIELDS = fieldnames(MDState)
const ATOM_DATA_FIELDS =
    (
        :id,
        :type,
        :mol,
        :coord,
        :vel,
    )

const GLOBAL_DATA_FIELDS =
    (
        :timestep,
        :box_type,
        :box_vectors,
        :origin,
        :pbc,
        :_hasfields
    )

"""
    getindex(state::MDState, key::Symbol)

Return an array of atomic properties corresponding to `key`, or `missing` if a property
    with that name is not found. Valid keys:
- `:id`: return a vector of atomic IDs
- `:type`: return a vector of atom types
- `:mol`: return a vector of molecule IDs
- `:coord`: return the particle coordinates, wrapped into the box
- `:vel`: return the particle velocities, if present
- `:element`: return the atomic element names, if present
- `:mass`: return the particle masses, if present
- any name of extra scalar or vector properties
"""
@inline function Base.getindex(buf::MDState, p::Symbol)
    if p in ATOM_DATA_FIELDS
        return getfield(buf, p)
    elseif p === :mass
        return masses(buf)
    elseif p === :element
        return element_names(buf)
    elseif haskey(buf._scalars, p)
        return buf._scalars[p]
    elseif haskey(buf._vectors, p)
        return buf._vectors[p]
    else
        return missing
    end
end

function Base.haskey(buf::MDState, p::Symbol)
    return p in ATOM_DATA_FIELDS ||
        (p === :mass && hasmass(buf)) ||
        (p === :element && has_element_name(buf)) ||
        haskey(buf._scalars, p) ||
        haskey(buf._vectors, p)
end

function Base.keys(buf::MDState)
    keylist = collect(ATOM_DATA_FIELDS)
    hasmass(buf) && push!(keylist, :mass)
    has_element_name(buf) && push!(keylist, :element)
    append!(keylist, keys(buf._scalars), keys(buf._vectors))
    return keylist
end

@inline function Base.get(buf::MDState, p::Symbol, default)
    if p in ATOM_DATA_FIELDS
        return getfield(buf, p)
    elseif p === :mass && !iszero(buf._hasfields & HASMASS_MASK)
        return p._mass
    elseif p === :element && !iszero(buf._hasfields & HASELEMENT_MASK)
        return p._element
    elseif haskey(buf._scalars, p)
        return buf._scalars[p]
    elseif haskey(buf._vectors, p)
        return buf._vectors[p]
    else
        default
    end
end

function Base.show(io::IO, state::MDState)
    outfmt = Format("""
    MDState with %d atoms""")
    format(
        io,
        outfmt,
        length(state.coord),
    )
    isempty(state._scalars) || print(
        io,
        ", extra scalar properties: ", keys(state._scalars),
    )
    isempty(state._vectors) || print(
        io,
        ", extra vector properties: ", keys(state._vectors),
    )
end

"""
    addproperty!(state::MDState; name, data::AbstractVecOrMat, allow_sharing=false)

Add a new property with the given `name` to `state` and initialize it with `data`. The new
    property name should not already be present in `state`.

If `data` is a vector, then the new scalar or vector property is added depending on
    `eltype(data)`. If `data` is a 3xN matrix, then a vector property is added.

If the flag `allow_sharing` is set to `true`, `state` stores a reference to `data` when
    possible. If the flag is set to `false` (default), an independent copy of `data` is
    added.
"""
function addproperty!(
    state::MDState;
    name, data::AbstractVecOrMat, allow_sharing::Bool=false,
)
    return addproperty!(state, name, data, allow_sharing)
end

function addproperty!(
    state::MDState,
    name::Union{Symbol, AbstractString, AbstractChar},
    data::AbstractVecOrMat,
    allow_sharing::Bool=false
)
    key = Symbol(name)
    if key in STATE_FIELDS ||
        key in (:mass, :vel, :element) ||
        haskey(state._scalars, key) ||
        haskey(state._vectors, key)
        throw(ArgumentError("Property $name already exists"))
    end
    return __addproperty_validkey!(state, key, data, allow_sharing)
end


function __addproperty_validkey!(
    state::MDState,
    key::Symbol,
    data::AbstractVector{<:Number},
    allow_sharing::Bool,
)
    if length(data) != length(state.coord)
        throw(DimensionMismatch(
            "expected $(length(state.coord))-element input, got length $(length(data))"
        ))
    end

    state._scalars[key] = allow_sharing ? data : Float64.(data)
    return state
end

function __addproperty_validkey!(
    state::MDState,
    key::Symbol,
    data::AbstractMatrix{Ta},
    ::Bool,
) where {Ta}
    if size(data) != (3, length(state.coord))
        nr, nc = size(data)
        throw(DimensionMismatch(
            "expected 3x$(length(state.coord)) matrix, got $(nr)x$(nc)"
        ))
    end

    state._vectors[key] = SVector{3,Float64}.(reinterpret(reshape, NTuple{3,Ta}, data))
    return state
end

function __addproperty_validkey!(
    state::MDState,
    key::Symbol,
    data::AbstractVector{<:Union{AbstractVector, NTuple{3,Number}}},
    allow_sharing::Bool,
)
    if length(data) != length(state.coord)
        throw(DimensionMismatch(
            "expected $(length(state.coord))-element input, got length $(length(data))"
        ))
    end

    state._vectors[key] = allow_sharing ? data : SVector{3,Float64}.(data)
    return state
end

function __addproperty_validkey!(
    state::MDState,
    key::Symbol,
    data::AbstractVector,
    ::Bool,
)
    if length(data) != length(state.coord)
        throw(DimensionMismatch(
            "expected $(length(state.coord))-element input, got length $(length(data))"
        ))
    end

    if all(x -> x isa Number, data)
        state._scalars[key] = data
    elseif all(x -> x isa AbstractVector, data)
        state._vectors[key] = data
    end
    return state
end

"""
    MDParticle

A type for iteration over MDState.
"""
struct MDParticle
    state::MDState
    idx::Int
end

@inline function Base.getindex(part::MDParticle, p::Symbol)
    return part.state[p][part.idx]
end

function Base.haskey(part::MDParticle)
    return haskey(part.state)
end

function Base.keys(part::MDParticle)
    return keys(part.state)
end

function Base.get(part::MDParticle, p::Symbol, default)
    return haskey(part, p) ? part[p] : default
end

function Base.show(io::IO, part::MDParticle)
    state = part.state
    outfmt = Format("""
    MDParticle: ID=%d, type=%d, mol=%d, r=[%.4g, %.4g, %.4g], v=[%.4g, %.4g, %.4g]""")
    format(
        io,
        outfmt,
        id_tag(part),
        type_tag(part),
        mol_tag(part),
        unwrapped_position(part)...,
        velocity(part)...,
    )
    isempty(state._scalars) || print(
        io,
        ", extra scalar properties: ", keys(state._scalars),
    )
    isempty(state._vectors) || print(
        io,
        ", extra vector properties: ", keys(state._scalars),
    )
end
