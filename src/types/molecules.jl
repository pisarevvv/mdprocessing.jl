struct Molecule
    state::MDState
    atom_weight::Vector{Float64}
    id::Int
    total_weight::Float64
    coord::SVector{3,Float64}
    vel::SVector{3,Float64}
    image::SVector{3,Int16}
    atom_inds::IndexList{Int}
end

struct Molecules<:AbstractVector{Molecule}
    parent::MDState
    id::Vector{Int}
    list::IndexListArray{1,Int}
    atom_weight::Vector{Float64}
end

Base.@propagate_inbounds function Molecules(system::MDState, weights::Vector{Float64})
    mol_ids = sort!(unique(system.mol))
    nmol = length(mol_ids)
    list = IndexListArray(; dims=(nmol,), nitems=natoms(system))
    for k in reverse(eachindex(system.mol))
        molid = system.mol[k]
        imol = searchsortedfirst(mol_ids, molid)
        push!(list[begin+imol-1], k) # cell list is zero-based
    end
    return Molecules(system, mol_ids, list, weights)
end

function Base.show(io::IO, mol::Molecules)
    outfmt = Format("""
    List of molecules (total %d)""")
    format(
        io,
        outfmt,
        length(mol),
    )
end

function Base.length(mol::Molecules)
    return length(mol.list)
end

function Base.size(mol::Molecules)
    return size(mol.list)
end

Base.@propagate_inbounds function Base.getindex(mol::Molecules, i::Integer)
    ind = i - one(i) # account for zero-based list indexing
    @boundscheck i in eachindex(mol.id) || BoundsError(mol, i)
    state = mol.parent
    atomvel = state.vel
    coord = state.origin
    vel = SVector(0.0, 0.0, 0.0)
    mass = 0.0
    natoms = 0
    boxsize = diag(boxvectors(state))
    weight = mol.atom_weight
    if iswrapped(state)
        let
            atomcoord = wrapped_positions(state)
            for iatom in mol.list[begin+ind]
                natoms += 1
                amass = weight[iatom]
                mass += amass
                if natoms == 1
                    coord = atomcoord[iatom]
                else
                    atom_disp = pbcvector(coord, atomcoord[iatom], boxsize, state.pbc)
                    coord += amass / mass * atom_disp
                end
                vel += atomvel[iatom] * amass
            end
        end
    else
        let
            atomcoord = unwrapped_positions(state)
            for iatom in mol.list[begin+ind]
                natoms += 1
                amass = weight[iatom]
                mass += amass
                if natoms == 1
                    coord = atomcoord[iatom]
                else
                    atom_disp = atomcoord[iatom] - coord
                    coord += amass / mass * atom_disp
                end
                vel += atomvel[iatom] * amass
            end
        end
    end
    vel /= mass

    offset_coord = coord - state.origin
    image = floor.(Int16, offset_coord ./ boxsize)
    coord -= boxvectors(state) * image
    atom_list = mol.list[ind]
    if iswrapped(state)
        image = zero.(image)
    end
    return Molecule(state, mol.atom_weight, mol.id[i], mass, coord, vel, image, atom_list)
end

Base.IndexStyle(::Type{Molecules}) = IndexLinear()

Base.eachindex(mol::Molecules) = Base.OneTo(length(mol.list))

function Base.show(io::IO, mol::Molecule)
    outfmt = Format("""Molecule #%d""")
    format(io, outfmt, mol.id)
    #join(io, mol.atoms, ' ')
end

Base.@propagate_inbounds function Base.iterate(mol::Molecule, iter_state...)
    atoms = mol.atom_inds
    next = iterate(atoms, iter_state...)
    if next === nothing
        return nothing
    end
    ind, iter_state = next
    return MDParticle(mol.state, ind), iter_state
end

Base.IteratorEltype(::Type{Molecule}) = Base.HasEltype()

Base.eltype(::Type{Molecule}) = MDParticle

Base.IteratorSize(::Type{Molecule}) = Base.HasLength()

Base.length(mol::Molecule) = length(mol.atom_inds)

"""
    molecules(system::MDState; weighing::Union{Symbol,AbstractString}=:mass)

Return an array-like object for molecules in `system`. `weighing` specifies if atoms have
    the same weight for computation of averages (`weighing = :geometry`) or vector of
    masses in `system` is used (`weighing = :mass`).
"""
function molecules(system::MDState; weighing=:mass)
    weigh = Symbol(weighing)
    if weigh === :geometry
        weights = ones(natoms(system))
    elseif weigh === :mass
        weights = system._mass
    else
        throw(ArgumentError("Unknown weighing scheme for molecules"))
    end
    return Molecules(system, weights)
end

"""
    wrapped_position(mol::Molecule)

Return the molecule center-of-mass coordinates vector wrapped into the simulation box.
"""
function wrapped_position(mol::Molecule)
    return mol.coord
end

"""
    unwrapped_position(mol::Molecule)

Return the unwrapped molecule center-of-mass coordinates vector.
"""
function unwrapped_position(mol::Molecule)
    return mol.coord + boxvectors(mol.state) * mol.image
end

"""
    map(fn, mol::Molecules)

Apply `fn` to each of the molecules in `mol`. The return value must be a `NamedTuple` of
    the form `(property1=val1, property2=val2, ...)`. The output of the mapping is `MDState`
    with the same box as `mol`'s parent state, where the particles are molecules in `mol`,
    the positions and velocities correspond to the center-of-mass parameters for the
    molecules, and `property1`, `property2` etc. are added to the list of additional
    properties.
"""
function Base.map(fn, mol::Molecules)
    state = mol.parent
    buf = MDState(
        ;
        box_vectors=boxvectors(state),
        box_type = boxtype(state),
        origin=state.origin,
        pbc=state.pbc,
    )
    buf.timestep = timestep(state)
    buf._hasfields = state._hasfields
    molcoord = buf.coord
    molvel = buf.vel
    update_coord!(buf, Iterators.repeated(SVector(0.0, 0.0, 0.0), length(mol)))
    for (idx, m) in enumerate(mol)
        molcoord[idx] = m.coord
        molvel[idx] = m.vel
        buf.id[idx] = m.id
        buf.mol[idx] = m.id
        buf._image[idx] = m.image
        buf._mass[idx] = hasmass(state) ? m.total_weight : 1.0
    end

    mapping = [fn(m) for m in mol]
    for key in keys(first(mapping))
        addproperty!(
            buf
            ;
            name=key, data=[getindex(item, key) for item in mapping], allow_sharing=true
        )
    end
    return buf
end
