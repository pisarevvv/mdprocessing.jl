function clear_state!(system::MDState)
    empty!(system.coord)
    empty!(system.vel)
    empty!(system.id)
    empty!(system.mol)
    empty!(system.type)
    empty!(system._image)
    empty!(system._mass)
    empty!(system._element)
    empty!(system._scalars)
    empty!(system._vectors)
    system.nreal = 0
    system.pbc = (true, true, true)
    system.timestep = 0
    system._hasfields = 0
    return system
end

"""
    natoms(system::MDState)

Return the number of atoms in the system.
"""
natoms(system::MDState) = system.nreal # length(system.coord)

"""
    timestep(system::MDState)

Return the number of the timestep of `system`.
"""
timestep(system::MDState) = system.timestep

"""
    boxtype(system::MDState)

Return if the box cell is `:orthogonal` or `:triclinic`.
"""
boxtype(system::MDState) = system.box_type == ORTHO_CELL ? :orthogonal : :triclinic

"""
    boxvectors(system::MDState)

Return the 3x3 matrix of box basis vectors.
"""
boxvectors(system::MDState) = system.box_vectors

"""
    boxorigin(system::MDState)

Return the vector of the box coordinate origin.
"""
boxorigin(system::MDState) = system.origin

"""
    pbcflags(system::MDState)

Return if the axes of the simulation box in `system` are under periodic boundary conditions.
"""
pbcflags(system::MDState) = system.pbc

"""
    iswrapped(system::MDState)

Return if the system has wrapped coordinates.
"""
function iswrapped(system::MDState)
    return iszero(system._hasfields & HASIMAGES_MASK)
end

"""
    wrapped_positions(system::MDState)

Return an array of particle coordinates in `system` wrapped into the simulation box.

See also: [`iswrapped`](@ref), [`wrapped_positions`](@ref)
"""
function wrapped_positions(system::MDState)
    return system.coord
end

"""
    unwrapped_positions(system::MDState)

Return an array of unwrapped particle coordinates in `system`, or wrapped coordinates if no
    information for unwrapping is available.

See also: [`iswrapped`](@ref), [`wrapped_positions`](@ref)
"""
function unwrapped_positions(system::MDState)
    boxvecs = boxvectors(system)
    coord = system.coord
    image = system._image
    return mappedarray((r, img) -> r + boxvecs * img, coord, image)
end

"""
    hasvelocity(system::MDState) -> Tuple{Bool,Bool,Bool}

Return if `system` contains information about particle velocities in `(x, y, z)`
    coordinates.
"""
function hasvelocity(system::MDState)
    return .!iszero.(system._hasfields .& (HASVX_MASK, HASVY_MASK, HASVZ_MASK))
end

"""
    velocities(system::MDState)

Return an array of particle velocities in `system`.

See also: [`hasvelocity`](@ref)
"""
function velocities(system::MDState)
    return system.vel
end

"""
    hasmass(system::MDState)

Return `true` if `system` contains masses information.
"""
function hasmass(system::MDState)
    return !iszero(system._hasfields & HASMASS_MASK)
end

"""
    masses(system::MDState)

Return the vector of masses in `system`. If masses are not present, then `missing` is
    returned.
"""
function masses(system::MDState)
    if hasmass(system)
        return system._mass
    else
        return missing
    end
end

"""
    has_element_name(system::MDState)

Return if `system` contains valid element name data.
"""
function has_element_name(system::MDState)
    return !iszero(system._hasfields & HASELEMENT_MASK)
end

"""
    element_names(system::MDState)

Return the vector of element names in `system`. If names are not present, then `missing` is
    returned.
"""
function element_names(system::MDState)
    if has_element_name(system)
        return system._element
    else
        return missing
    end
end

"""
    id_tags(system::MDState)

Return atomic IDs for atoms in `system`.
"""
id_tags(system::MDState) = system.id

"""
    mol_tags(system::MDState)

Return molecule IDs for atoms in `system`.
"""
mol_tags(system::MDState) = system.mol

"""
    type_tags(system::MDState)

Return type IDs for atoms in `system`.
"""
type_tags(system::MDState) = system.type

"""
    atom_index(p::MDParticle)

Return the index of particle `p` in the `MDState` it belongs to.
"""
atom_index(p::MDParticle) = p.idx

"""
    wrapped_position(p::MDParticle)

Return the particle coordinates vector wrapped into the simulation box.
"""
function wrapped_position(p::MDParticle)
    return wrapped_positions(p.state)[p.idx]
end

"""
    unwrapped_position(p::MDParticle)

Return the unwrapped particle coordinates vector.
"""
function unwrapped_position(p::MDParticle)
    return unwrapped_positions(p.state)[p.idx]
end

"""
    velocity(p::MDParticle)

Return the velocity of particle `p`.
"""
function velocity(p::MDParticle)
    return velocities(p.state)[p.idx]
end

"""
    id_tag(p::MDParticle)

Return atomic ID for the particle `p`.
"""
function id_tag(p::MDParticle)
    return id_tags(p.state)[p.idx]
end

"""
    mol_tag(p::MDParticle)

Return molecule ID for the particle `p`.
"""
function mol_tag(p::MDParticle)
    return mol_tags(p.state)[p.idx]
end

"""
    type_tag(p::MDParticle)

Return type ID for the particle `p`.
"""
function type_tag(p::MDParticle)
    return type_tags(p.state)[p.idx]
end

"""
    volume(system::MDState)

Return the volume of the simulation box of `system`.
"""
function volume(system::MDState)
    return system |> boxvectors |> det |> abs
end

"""
    set_timestep!(system::MDState, step::Integer)

Set the timestep number for `system` to `step`.
"""
function set_timestep!(system::MDState, step::Integer)
    system.timestep = step
end

"""
    set_typenames!(system::MDState, typename)

Set element names in `system` according to the types information.

# Arguments:
- `system`: the modified system
- `typename`: a collection indexable by integer keys. If the type of *i*-th particle is *t*,
    then its element tag is set to `typename[t]`.

# Returns:
The updated vector of particle element names.

See also: [`set_elementmasses!`](@ref)
"""
function set_typenames!(system::MDState, typenames)
    for t in system.type
        if !(t in keys(typenames))
            throw(ArgumentError("No name specified for type $t."))
        end
    end
    for (ind, t) in pairs(system.type)
        system._element[ind] = typenames[t]
    end

    system._hasfields |= HASELEMENT_MASK
    return system._element
end

"""
    setmasses!(system::MDState, masses::AbstractArray)

Copy `masses` as a particle mass array in `system`.

# Arguments:
- `system`: the modified system
- `masses`: an array of desired particle masses. The dimensions must be
    `natoms(system) x 1`.

# Returns:
The updated vector of particle masses.

See also: [`set_typemasses!`](@ref), [`set_elementmasses!`](@ref), [`set_typenames!`](@ref)
"""
function setmasses!(system::MDState, mass::AbstractArray)
    @boundscheck let
        ax = size(mass, 1), size(mass, 2)
        if !(ax[1] == natoms(system) && ax[2] == 1)
            throw(
                DimensionMismatch(
                    "An array of masses has to have dimensions `(natoms, 1)`, got $ax."
                )
            )
        end
    end

    system._hasfields |= HASMASS_MASK
    system._mass .= mass
end

"""
    set_typemasses!(system::MDState, typemass)

Set masses in `system` according to the types information.

# Arguments:
- `system`: the modified system
- `typemass`: a collection indexable by integer keys. If the type of *i*-th particle is *t*,
    then its mass is set to `typemass[t]`.

# Returns:
The updated vector of particle masses.

See also: [`set_masses!`](@ref), [`set_elementmasses!`](@ref), [`set_typenames!`](@ref)
"""
function set_typemasses!(system::MDState, typemass)
    for t in system.type
        if !(t in keys(typemass))
            throw(ArgumentError("No mass specified for type $t."))
        end
    end
    for (ind, t) in pairs(system.type)
        system._mass[ind] = typemass[t]
    end

    system._hasfields |= HASMASS_MASK
    return system._mass
end

"""
    set_elementmasses!(system::MDState, elmass)

Set masses in `system` according to the element name information.
"""
function set_elementmasses!(system::MDState, elmass)
    if !has_element_name(system)
        throw(ArgumentError("Element names are not specified."))
    end
    for t in system._element
        if !(t in keys(elmass))
            throw(ArgumentError("No mass specified for type $t."))
        end
    end
    for (ind, t) in pairs(system._element)
        system._mass[ind] = elmass[t]
    end

    system._hasfields |= HASMASS_MASK
    return system._mass
end

function set_elementmasses!(system::MDState, elmass::NamedTuple)
    if !has_element_name(system)
        throw(ArgumentError("Element names are not specified."))
    end
    for t in system._element
        if !(Symbol(t) in keys(elmass))
            throw(ArgumentError("No mass specified for type $t."))
        end
    end
    for (ind, t) in pairs(system._element)
        system._mass[ind] = elmass[Symbol(t)]
    end

    system._hasfields |= HASMASS_MASK
    return system._mass
end
