### Array interface

"""
    getindex(c::Clusters, i::Int)

Return struct `Cluster` by index `i` of the cluster in `Clusters`.
"""
Base.@propagate_inbounds function Base.getindex(c::Clusters, i::Integer)
    atom_indices = c.list[begin + i - one(i)]

    return Cluster(i, atom_indices, c.state)
end

Base.eachindex(c::Clusters) = Base.OneTo(length(c.list))

function Base.size(c::Clusters)
    return size(c.list)
end

## MDProcessing API

"""
    parent_state(clust::Clusters)

Return the state on which `clust` is based.
"""
parent_state(clust::Clusters) = clust.state

"""
    add_clusters!(system::MDState; bond_distance::Real[, predicate, sort::Bool=true])

Find clusters in `system` according to `bond_distance` and `predicate`. `predicate` must be
    a function `ParticlePair -> Bool` that filters which neighbor pairs within
    `bond_distance` must be included as connections. Then, add property `:cluster_id` to
    `system` so that the particles belonging to the same cluster have the same `cluster_id`.
    If `sort === true`, then `cluster_id` are sorted from largest to smallest cluster.

See also: [`clusters`](@ref)
"""
function add_clusters!(
    system::MDState;
    bond_distance::Real, predicate=nothing, sort::Bool=true
)
    if predicate === nothing
        clust = clusters(system, bond_distance; sort)
    else
        pairs = filter(predicate, neighbor_pairs(system, bond_distance))
        clust = clusters(system, pairs; sort)
    end
    addproperty!(system; name=:cluster_id, data=clust.id, allow_sharing=true)
    return system
end

"""
    same_cluster(pair::ParticlePair, clusters::Clusters)

Return `true` if two atoms in the pair are in the same cluster, `false` otherwise.
"""
function same_cluster(pair::ParticlePair, c::Clusters)
    i1, i2 = atom_indices(pair)
    return c.id[i1] == c.id[i2]
end

"""
    same_cluster(pair::NTuple{2,MDParticle}, clusters::Clusters)

Return `true` if two atoms are in the same cluster, `false` otherwise.
"""
function same_cluster(pair::NTuple{2,MDParticle}, c::Clusters)
    p1, p2 = pair
    i1 = pair[1].idx
    i2 = pair[2].idx
    return p1.state === p2.state === c.state && c.id[i1] == c.id[i2]
end

"""
    get_cluster(clust::Clusters, part::MDParticle)

Return the cluster `part` belongs to.
"""
function get_cluster(clust::Clusters, part::MDParticle)
    if part.state === clust.state
        clust_id = clust.id[part.idx]
        return clust[clust_id]
    else
        return nothing
    end
end

atom_indices(clust::Cluster) = clust.atom_indices

function Base.in(part::MDParticle, clust::Cluster)
    return part.state === clust.state && part.idx in atom_indices(clust)
end

Base.@propagate_inbounds function Base.iterate(clust::Cluster)
    atoms = clust.atom_indices
    next = iterate(atoms)
    if next === nothing
        return nothing
    end
    ind, iter_state = next
    return MDParticle(clust.state, ind), iter_state
end

Base.@propagate_inbounds function Base.iterate(clust::Cluster, iter_state)
    atoms = clust.atom_indices
    next = iterate(atoms, iter_state)
    if next === nothing
        return nothing
    end
    ind, iter_state = next
    return MDParticle(clust.state, ind), iter_state
end

Base.IteratorEltype(::Type{Cluster}) = Base.HasEltype()

Base.eltype(::Type{Cluster}) = MDParticle

Base.IteratorSize(::Type{Cluster}) = Base.HasLength()

Base.length(clust::Cluster) = length(clust.atom_indices)

natoms(clust::Cluster) = length(clust.atom_indices)
