"""
    parent_state(mols::Molecules)

Return the state on which `mols` is based.
"""
parent_state(mols::Molecules) = mols.parent

"""
    atom_indices(mol::Molecule)

Return an iterable with indices of atoms belonging to molecule `mol`.
"""
atom_indices(mol::Molecule) = mol.atom_inds

"""
    position_offsets(mol::Molecule)

Return an iterable of atomic coordinate differences from the molecule center.
"""
Base.@propagate_inbounds function position_offsets(mol::Molecule)
    state = mol.state
    pbc = state.pbc

    if iswrapped(state)
        let
            boxsize = diag(boxvectors(state))
            atomcoords = wrapped_positions(state)
            centercoord = wrapped_position(mol)
            return (
                pbcvector(centercoord, atomcoords[i], boxsize, pbc)
                for i in atom_indices(mol)
            )
        end
    else
        let
            atomcoords = unwrapped_positions(state)
            centercoord = unwrapped_position(mol)
            return (atomcoords[i] - centercoord for i in atom_indices(mol))
        end
    end
end

"""
    id_tags(mol::Molecules)

Return molecule IDs for molecules in `mol`.
"""
function id_tags(mol::Molecules)
    return mol.id
end

"""
    mol_tags(mol::Molecules)

Return molecule IDs for molecules in `mol`.
"""
function mol_tags(mol::Molecules)
    return mol.id
end

"""
    velocities(mols::Molecules)

Return an iterable of molecular velocities in `mols`. The molecular velocities are computed
    from atom velocities for each molecule `mol` taken with weights given by
    `atom_weights(mol)`.

See also: [`atom_weights`](@ref)
"""
function velocities(mols::Molecules)
    return mappedarray(eachindex(mols)) do ind
        state = mols.parent
        vels = velocities(state)
        atom_inds = mols.list[begin+ind-one(ind)]
        weights = mols.atom_weight
        vel = SVector(0.0, 0.0, 0.0)
        m = 0.0
        for i in atom_inds
            w = weights[i]
            m += w
            vel += w * vels[i]
        end
        vel /= m + 1e-100
        return vel
    end
end

"""
    atom_weights(mol::Molecule)

Return an iterable over the weights of atoms in `mol` used to compute averages. They may be
    equal to atom masses in the parent `MDState` or all equal to unity.
"""
function atom_weights(mol::Molecule)
    return (mol.atom_weight[i] for i in atom_indices(mol))
end

"""
    id_tags(mol::Molecule)

Return atom IDs for atoms in `mol`.
"""
function id_tags(mol::Molecule)
    ids = id_tags(mol.state)
    return (ids[i] for i in atom_indices(mol))
end

"""
    id_tag(mol::Molecule)

Return molecule IDs for `mol`.
"""
id_tag(mol::Molecule) = mol.id

"""
    mol_tag(mol::Molecule)

Return molecule IDs for `mol`.
"""
mol_tag(mol::Molecule) = mol.id

"""
    natoms(mol::Molecule)

Return number of atoms in molecule.
"""
natoms(mol::Molecule) = length(mol.atom_inds)

"""
    velocity(mol::Molecule)

Return the velocity of `mol`, computed according to the weighing scheme.

See also: [`atom_weights`](@ref)
"""
velocity(mol::Molecule) = mol.vel

"""
    atom_velocities(mol::Molecule)

Return an iterable object over atomic velocities in `mol`.
"""
function atom_velocities(mol::Molecule)
    return (velocities(mol.state)[i] for i in atom_indices(mol))
end

"""
    molecule_weights(mols::Molecules)

Return an array-like object of total weights of molecules in `mol`. If the parent state has
    no particle mass information, or `mols` is created with `weighing=:geometry`, all atom
    weights are assumed to be 1, so that the molecule weight denoted the number of atoms.
"""
function molecule_weights(mols::Molecules)
    return mappedarray(eachindex(mols)) do ind
        atom_inds = mols.list[begin+ind-one(ind)]
        weights = mols.atom_weight
        return sum(weights[i] for i in atom_inds)
    end
end

molecule_weight(mol::Molecule) = mol.total_weight
