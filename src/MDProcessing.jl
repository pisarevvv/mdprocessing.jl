module MDProcessing

using Base.Sort

using AxisKeys
using LinearAlgebra
using MappedArrays
using Printf: @format_str, Format, format
using StaticArrays
using Statistics

export MDState
export read_dump, read_dump!
export addproperty!, update_coord!, clear_state!

export timestep, natoms, boxtype, boxvectors, boxorigin, pbcflags, volume
export id_tags, mol_tags, type_tags
export iswrapped, wrapped_positions, unwrapped_positions
export wrapped_position, unwrapped_position, velocity
export id_tag, mol_tag, type_tag, atom_index

export hasmass, masses, has_element_name, element_names
export setmasses!, set_typemasses!, set_elementmasses!, set_typenames!
export hasvelocity, velocities

export molecules, parent_state, molecule_weight, molecule_weights
export position_offsets, atom_indices, atom_weights, atom_velocities

export neighbor_pairs, pbcvector, pbcdist, pbcdist2

export gyration_radius, gyration_tensor, inertia_tensor
export asphericity, acylindricity, shape_anisotropy, anisotropy_factors
export rdf, intra_rdf, rdf!, distance_range, type_pairs
export property_distance_correlation
export molecule_planes, molecule_lines
export cc, center_of_mass, fit_normal, fit_line
export displacements, msd
export traj_average, traj_correlate
export Autocorr

export clusters, same_cluster, co_directed, get_cluster#, norm_score, co_directed_r,
export add_clusters!

const BEST_NATOMS_PER_CELL = Ref(16)

include("types/md_state.jl")
include("types/linked_list.jl")
include("types/molecules.jl")
include("types/clusters.jl")
include("algorithms/neighbor_search/cell_list.jl")
include("algorithms/neighbor_search/neigh_search.jl")
include("algorithms/disjoint_sets.jl")
include("types/particle_pair.jl")
include("types/neigh_pairs.jl")
include("iteration/md_state.jl")
include("interface/mdstate.jl")
include("interface/molecules.jl")
include("interface/clusters.jl")
include("calc_properties/shape.jl")
include("calc_properties/spatial_correlation.jl")
include("math.jl")
include("md_statistics.jl")
include("io_utils.jl")
include("trajectory.jl")
include("autocorr/autocorr.jl")
include("calc_properties/cluster_funcs.jl")

end # module MDProcessing
