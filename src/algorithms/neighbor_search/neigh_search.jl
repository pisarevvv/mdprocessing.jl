"""
    neigh_atoms(cell_list, cell_inds)

Return the unsorted array with indices of atoms in cell `cell_inds` and the neighboring
    cells. `cell_inds` may be a tuple or `CartesianIndex`.
"""
function neigh_atoms(
    cell_list::CellList,
    cell_inds::Union{NTuple{3,Integer},CartesianIndex{3}},
)
    cells = cell_list.cells
    list_size = size(cells)
    ix, iy, iz = Tuple(cell_inds)

    neighs = @inbounds (
        ind for (jx, jy, jz) in neigh_indices(cell_list.stencil, (ix, iy, iz), list_size)
        for ind in cell_list[jx, jy, jz]
    )
    return neighs
end

function neigh_indices(stencil::Stencil, (ix, iy, iz), list_size)
    return (clamp.((ix, iy, iz) .+ Tuple(cov_ind), list_size) for cov_ind in stencil.cover)
end
