"""
    mutable struct DisjointSets{T<:Integer}

store id of each element and count of disjoint sets.
"""
mutable struct DisjointSets{T<:Integer}
    id::Vector{T}
    count::T

    DisjointSets{T}(n::Integer) where T = new{T}(1:n, n)
end

function DisjointSets(n::T) where {T<:Integer}
    return DisjointSets{T}(n)
end

"""
    DisjointSets(n::Integer, connections)

Create a `DisjointSets` structure with `n` nodes with a given iterable `connections` object
    that contains index pairs (or other iterables where first two elements are two indices).
"""
function DisjointSets(n::Integer, connections)
    qu = DisjointSets(n)
    foreach(connections) do conn
        p, q = conn
        connect!(qu, p, q)
    end
    return qu
end

"""
    ncomponents(q::DisjointSets)

Return the number of components in the disjoint set represented by `q`.
"""
ncomponents(q::DisjointSets) = q.count

"""
    find(q::DisjointSets, p::Integer)

Return the canonical element of the set containing element `p`.
"""
function find(q::DisjointSets, p::Integer)
    id = q.id

    while p != id[p]
        newp = id[p]
        id[p] = id[newp]
        p = newp
    end

    return p
end

"""
    connected(qf::DisjointSets, p, r)

Return `true` if the two elements are in the same set.
"""
function connected(q::DisjointSets, p, r)
    checkbounds(q.id, p)
    checkbounds(q.id, r)
    @inbounds return find(q, p) == find(q, r)
end

"""
    connect!(qu::DisjointSets, p, r)

Merge the set containing element `p` with the set containing element `r`.
"""
function connect!(qu::DisjointSets, p, r)
    id = qu.id
    rootP = find(qu, p)
    rootR = find(qu, r)
    if rootR == rootP
        return
    end
    qu.count -= 1
    if rootP < rootR
        id[rootR] = rootP
    else
        id[rootP] = rootR
    end
    return
end

"""
    root_nodes(q::DisjointSets)

Return root nodes for each element as an array.
"""
function root_nodes(q::DisjointSets)
    roots = [find(q, i) for i in eachindex(q.id)]
    return roots
end
