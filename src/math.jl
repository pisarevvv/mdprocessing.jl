"""
    clamp(ind, imax)

Return `ind` if it is in the range `0:imax`, or wrap it to that range by adding or
    subtracting `imax`
"""
function clamp(ind, imax)
    if ind < 0
        ind + imax
    elseif ind < imax
        ind
    else
        ind - imax
    end
end

"""
    mag2(vec)

Return the squared magnitude (sum of the squares of the components) of `vec`.
"""
mag2(vec) = sum(x->x^2, vec)

"""
    dot3(a, b, c)

Return the scalar-vector product `dot(a, cross(b, c))` of length-3 vectors.
"""
Base.@propagate_inbounds function dot3(a, b, c)
    @boundscheck if !(length(a) == length(b) == length(c) == 3)
        throw(DimensionMismatch("length-3 vectors expected in dot3"))
    end

    @inbounds prod = a[1] * (b[2] * c[3] - b[3] * c[2]) +
        a[2] * (b[3] * c[1] - b[1] * c[3]) +
        a[3] * (b[1] * c[2] - b[2] * c[1])
    return prod
end

function kin_energy(system::MDState)
    m = system._mass
    v = system.vel
    return sum(i -> m[i] * v[i]^2, eachindex(m, v)) / 2
end

"""
    pbcvector(r1, r2, size)

Return the vector `r2 - r1`, assuming they are in an orthogonal box with sides `size`
    under periodic boundary conditions.
"""
function pbcvector(
    r1::StaticVector{3,T}, r2::StaticVector{3,T}, size::StaticVector{3},
) where {T}
    Δr = (r2 - r1)
    halfsize = size / 2
    Δr = ifelse.(Δr .>= halfsize, Δr - size, Δr)
    Δr = ifelse.(Δr .< -halfsize, Δr + size, Δr)

    return Δr
end

"""
    pbcvector(r1, r2, size, pbc)

Return the vector `r2 - r1`, assuming they are in an orthogonal box with sides `size`. `pbc`
    specifies if the direction has periodic boundary conditions.
"""
function pbcvector(
    r1::StaticVector{3,T},
    r2::StaticVector{3,T},
    size::Union{StaticVector{3}, NTuple{3}},
    pbc::Union{StaticVector{3,Bool}, NTuple{3,Bool}}
) where {T}
    Δr = (r2 - r1)
    boxsize = SVector(size)
    halfsize = boxsize / 2
    Δr = ifelse.(pbc, ifelse.(Δr .>= halfsize, Δr - boxsize, Δr), Δr)
    Δr = ifelse.(pbc, ifelse.(Δr .< -halfsize, Δr + boxsize, Δr), Δr)

    return (Δr)
end

"""
    pbcvector(pair::ParticlePair)

Return the vector ``r_j - r_i`` in a pair `(i, j)`, with account of the periodic boundary
    conditions in the box where the pair belongs.
"""
function pbcvector(pair::ParticlePair)
    return pair.r12
end

"""
    pbcdist(r1::AbstractVector, r2::AbstractVector, size::Union{AbstractVector,Tuple})

Compute the distance between `r1` and `r2` in a box with size `size` assuming periodic
    boundary conditions.
"""
Base.@propagate_inbounds function pbcdist(
    r1::AbstractVector{T}, r2::AbstractVector{T}, size
) where {T<:Real}
    @boundscheck let ax1 = keys(r1), ax2 = keys(r2), ax3 = keys(size)
        if !(ax1 == ax2 == ax3)
            throw(DimensionMismatch("all inputs to pbcdist must have the same indices"))
        elseif length(ax1) != 3
            throw(DimensionMismatch("all inputs to pbcdist must be of length 3"))
        end
    end
    mag2 = zero(float(T))
    for (r1i, r2i, Li) in zip(r1, r2, size)
        dri = (r2i - r1i) % Li
        if dri > Li / 2
            dri -= Li
        elseif dri < -Li / 2
            dri += Li
        end
        mag2 += dri^2
    end
    return sqrt(mag2)
end

function pbcdist(
    r1::StaticVector{3,T}, r2::StaticVector{3,T}, size::StaticVector{3}
) where {T<:Real}
    Δr = pbcvector(r1, r2, size)
    return norm(Δr)
end

"""
    pbcdist(pair::ParticlePair)

Return the magnitude of the vector `rⱼ - rᵢ` in a pair `(i, j)`, with account of the
    periodic boundary conditions in the box where the pair belongs.
"""
function pbcdist(pair::ParticlePair)
    return sqrt(pair.sqdist)
end

function pbcdist2(
    r1::StaticVector{3,T}, r2::StaticVector{3,T}, size::StaticVector{3}
) where {T<:Real}
    Δr = pbcvector(r1, r2, size)
    return mag2(Δr)
end

"""
    pbcdist2(pair::ParticlePair)

Return the square of the magnitude of the vector `rⱼ - rᵢ` in a pair `(i, j)`, with account
    of the periodic boundary conditions in the box where the pair belongs.
"""
function pbcdist2(pair::ParticlePair)
    return pair.sqdist
end

"""
    cc(r1::AbstractVector, r2::AbstractVector)

Return correlation coefficient (cosine of the angle) between two vectors
"""
Base.@propagate_inbounds function cc(
    r1::AbstractVector{<:Real},
    r2::AbstractVector{<:Real}
)
    @boundscheck if !(keys(r1) == keys(r2))
        throw(DimensionMismatch("Axes must match for correlation coefficient"))
    end
    q = mag2(r1)
    w = mag2(r2)
    return dot(r1, r2) / sqrt(q * w)
end

function cc(r1::V, r2::V) where {V<:StaticVector{3,<:Real}}
    q = mag2(r1)
    w = mag2(r2)
    return dot(r1, r2) / sqrt(q * w)
end

"""
    integrate_trap!(runsum::AbstractVector, input::AbstractVector, dx::Number)

Fill `runsum` with the running integral values of `input`. The integration is performed by
    the trapezoid method, and `input` should contain the values of function at points
    separated by `dx`.
"""
function integrate_trap!(runsum::AbstractVector, input::AbstractVector, dx::Number)
    runsum[begin] = 0
    for n in firstindex(runsum)+1:lastindex(runsum)
        runsum[n] = runsum[n-1] + 0.5 * dx * (input[n-1] + input[n])
    end
    return runsum
end

"""
    integrate_trap(input::AbstractVector, dx::Number)

Return an array of running integral values of `input`. The integration is performed by
    the trapezoid method, and `input` should contain the values of function at points
    separated by `dx`.
"""
function integrate_trap(input::AbstractVector{T}, dx::Number=one(T)) where {T<:Number}
    runsum = similar(input, float(T))
    return integrate_trap!(runsum, input, dx)
end

"""
    center_of_mass(coords[; weights])

Compute the center of mass for a set of `coords` weighted by `weights`. If `weights` is not
    provided, then the function returns the geometric center.
"""
function center_of_mass(coords; weights=Iterators.repeated(true))
    (w0, r0), rest = Iterators.peel(zip(weights, coords))
    wtotal = w0
    wr = w0 * r0
    for (w, r) in rest
        if ismutable(wr)
            wr .+= w .* r
        else
            wr += w * r
        end
        wtotal += w
    end
    if ismutable(wr)
        wr ./= wtotal
    else
        wr /= wtotal
    end
    return wr
end

"""
    fit_normal(coords[; weights, center])

Compute the normal to the plane inscribed by the least squares method in the point cloud
    defined by `coords` passing through `center`. If `weights` are provided, then the
    weighted least squares solution is computed. If `center` is not provided, the center
    of mass is used.
"""
function fit_normal(
    coords
    ;
    weights=Iterators.repeated(true),
    center=center_of_mass(coords; weights)
)
    p = center
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    ncoord = 0
    wtotal = 0.0
    for (w, r) in zip(weights, coords)
        rx, ry, rz = r[1] - p[1], r[2] - p[2], r[3] - p[3]
        rxx += w * rx * rx
        ryy += w * ry * ry
        rzz += w * rz * rz
        rxy += w * rx * ry
        rxz += w * rx * rz
        ryz += w * ry * rz
        ncoord += 1
        wtotal += w
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz) / ncoord

    vals, vectors = eigen(gtens)
    nor = vectors[:, 1]
    if ncoord > 1
        r1, r2 = coords
        sr1 = SVector(r1[1] - p[1], r1[2] - p[2], r1[3] - p[3])
        sr2 = SVector(r2[1] - p[1], r2[2] - p[2], r2[3] - p[3])
        sign = dot3(sr1, sr2, nor)
        if sign < 0
            nor = -nor
        end
    end
    return nor
end

"""
    fit_normal(mol::Molecule)

Compute the normal to the plane inscribed by the least squares method in the point cloud.
"""
function fit_normal(mol::Molecule)
    gtens = gyration_tensor(mol)

    vals, vectors = gtens
    nor = vectors[:, 1]
    if length(mol) > 1
        r1, r2 = position_offsets(mol)
        sign = dot3(r1, r2, nor)
        if sign < 0
            nor = -nor
        end
    end
    return nor
end

"""
    fit_line(coords[; weights, center=mean(coords)])

Compute the best-fit straight-line approximation to the point cloud in the sense of total
    least squares.
"""
function fit_line(
    coords,
    ;
    weights=Iterators.repeated(true),
    center=center_of_mass(coords; weights)
)
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    ncoord = 0
    for (w, r) in zip(weights, coords)
        rx, ry, rz = r[1] - center[1], r[2] - center[2], r[3] - center[3]
        rxx += w * rx * rx
        ryy += w * ry * ry
        rzz += w * rz * rz
        rxy += w * rx * ry
        rxz += w * rx * rz
        ryz += w * ry * rz
        ncoord += 1
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz) / ncoord

    vals, vectors = eigen(gtens)
    dir = vectors[:, 3]
    if ncoord > 1
        imin = imax = 0
        dmin = Inf
        dmax = -Inf
        for (i, r) in enumerate(coords)
            d = dot(r - center, dir)
            if d < dmin
                imin, dmin = i, d
            end
            if d > dmax
                imax, dmax = i, d
            end
        end
        if imax < imin
            dir = -dir
        end
    end
    return dir
end

"""
    fit_line(mol::Molecule)

Compute the best-fit straight-line approximation to the coordinates of atoms in `mol` in
    the sense of total least squares.
"""
function fit_line(mol::Molecule)
    gtens = gyration_tensor(mol)

    vals, vectors = gtens
    dir = vectors[:, 3]
    if length(mol) > 1
        imin = imax = 0
        dmin = Inf
        dmax = -Inf
        for (i, r) in enumerate(position_offsets(mol))
            d = dot(r, dir)
            if d < dmin
                imin, dmin = i, d
            end
            if d > dmax
                imax, dmax = i, d
            end
        end
        if imax < imin
            dir = -dir
        end
    end
    return dir
end
