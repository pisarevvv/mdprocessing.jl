"""
    co_directed(system::MDState; bond_distance=4.5, cc_thresh=0.95, sort=true)

Return an array-like object for atom clusters in `system`. Find co-directed molecules at a
    distance less than `bond_distance`. `cc_thresh` is the threshold absolute value of
    correlation coefficient for which a pair is counted.
"""
function co_directed(system::MDState; bond_distance=4.5, cc_thresh=0.95, sort=true)
    molsystem = molecule_planes(system)
    normal_vecs = molsystem[:normal]

    ppairs = filter(neighbor_pairs(molsystem, bond_distance)) do ppair
        p, q = atom_indices(ppair)
        norm1 = normal_vecs[p]
        norm2 = normal_vecs[q]
        scalar_product = dot(norm1, norm2)
        return abs(scalar_product) > cc_thresh
    end
    clusters(molsystem, ppairs; sort)
end

"""
    co_directed_r(system::MDState, n::Int64; bond_distance=10, coeff=0.95)

Compute and return list of numbers of co-directed molecules from system::MDState in each of n ranges.
"""
function co_directed_r(system::MDState, n::Int64; bond_distance=10, coeff=0.95)
    dr = 0
    stat = zeros(Float64, n, 2)
    for i in 1:n
        dr += bond_distance/n;
        stat[i, 1] = dr
        stat[i, 2] = length(co_directed(system; bond_distance = dr, cc_thresh = coeff))
    end
    return stat
end
