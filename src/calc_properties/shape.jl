"""
    gyration_tensor(mol::Molecule)

Return the eigendecomposition of the  gyration tensor for a given molecule. If the molecule
    center is computed as the center of mass, then the gyration tensor computation takes
    into account atom masses. For molecules with geometry center, the tensor is computed
    using only the atomic positions.

See also: [`inertia_tensor`](@ref).
"""
function gyration_tensor end

function gyration_tensor(mol::Molecule)
    rs = position_offsets(mol)
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    for (m, r) in zip(atom_weights(mol), rs)
        rxx += m * r[1] * r[1]
        ryy += m * r[2] * r[2]
        rzz += m * r[3] * r[3]
        rxy += m * r[1] * r[2]
        rxz += m * r[1] * r[3]
        ryz += m * r[2] * r[3]
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz)

    vals, vectors = eigen(gtens)
    vals /= mol.total_weight
    if det(vectors) < 0
        vectors = -vectors
    end

    return Eigen(vals, vectors)
end

"""
    gyration_radius(mol::Molecule)

Return the radius of gyration for a given molecule. If the molecule center is computed as
    the center of mass, then the gyration radius takes into account atom masses. For
    molecules with geometry center, the tensor is computed using only the atomic positions.

See also: [`gyration_tensor`](@ref).
"""
function gyration_radius(mol::Molecule)
    rs = position_offsets(mol)
    rxx = ryy = rzz = 0.0
    for (m, r) in zip(atom_weights(mol), rs)
        rxx += m * r[1] * r[1]
        ryy += m * r[2] * r[2]
        rzz += m * r[3] * r[3]
    end
    return sqrt((rxx + ryy + rzz) / mol.total_weight)
end

"""
    asphericity(mol::Molecule)

Compute the asphericity factor
    `b = [R₃² - 0.5(R₁² + R₂²)] / Rg²` where `R₁² ≤ R₂² ≤ R₃²` are the gyration tensor
    eigenvalues, `Rg² = R₁² + R₂² + R₃²` is the squared radius of gyration.

See also: [`gyration_tensor`](@ref), [`gyration_radius`](@ref), [`acylindricity`](@ref),
    [`shape_anisotropy`](@ref).
"""
function asphericity(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    rgsq = sum(gvals)
    return (gvals[3] - (gvals[1] + gvals[2]) * 0.5) / rgsq
end

"""
    acylindricity(mol::Molecule)

Compute the acylindricity factor
    `c = (R₂² - R₁²) / Rg²` where `R₁² ≤ R₂² ≤ R₃²` are the gyration tensor eigenvalues,
    `Rg² = R₁² + R₂² + R₃²` is the squared radius of gyration.

See also: [`gyration_tensor`](@ref), [`gyration_radius`](@ref), [`asphericity`](@ref),
    [`shape_anisotropy`](@ref).
"""
function acylindricity(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    rgsq = sum(gvals)
    return (gvals[2] - gvals[1]) / rgsq
end

"""
    shape_anisotropy(mol::Molecule)

Compute the shape anisotropy factor
    `κ² = 1.5(R₁⁴ + R₂⁴ + R₃⁴) / Rg⁴ - 0.5` where `R₁² ≤ R₂² ≤ R₃²` are the gyration tensor
    eigenvalues, `Rg² = R₁² + R₂² + R₃²` is the squared radius of gyration.

See also: [`gyration_tensor`](@ref), [`gyration_radius`](@ref), [`asphericity`](@ref),
    [`acylindricity`](@ref).
"""
function shape_anisotropy(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    return 1.5 * sum(x->x^2, gvals) / sum(gvals)^2 - 0.5
end

"""
    anisotropy_factors(mol::Molecule)

Return the tuple of asphericity, acylindricity and shape anisotropy factors.

See also: [`asphericity`](@ref), [`acylindricity`](@ref), [`shape_anisotropy`](@ref),
    [`gyration_tensor`](@ref).
"""
function anisotropy_factors(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    rgsq = sum(gvals)
    b = (gvals[3] - (gvals[1] + gvals[2]) * 0.5) / rgsq
    c = (gvals[2] - gvals[1]) / rgsq
    ksq = b^2 + 0.75 * c^2
    return (asphericity=b, acilindricity=c, shape_anisotropy=ksq)
end

"""
    inertia_tensor(mol::Molecule)

Return the eigendecomposition of the inertia tensor for a given molecule.

See also: [`gyration_tensor`](@ref).
"""
function inertia_tensor(mol::Molecule)
    rs = position_offsets(mol)
    Ixx = Iyy = Izz = Ixy = Ixz = Iyz = 0.0
    for (m, r) in zip(atom_weights(mol), rs)
        rxx, ryy, rzz = r.^2
        Ixx += m * (ryy + rzz)
        Iyy += m * (rxx + rzz)
        Izz += m * (rxx + ryy)
        Ixy += m * r[1] * r[2]
        Ixz += m * r[1] * r[3]
        Iyz += m * r[2] * r[3]
    end
    gtens = SMatrix{3,3}(Ixx, -Ixy, -Ixz, -Ixy, Iyy, -Iyz, -Ixz, -Iyz, Izz)

    vals, vectors = eigen(gtens)
    if det(vectors) < 0
        vectors = -vectors
    end
    return Eigen(vals, vectors)
end
