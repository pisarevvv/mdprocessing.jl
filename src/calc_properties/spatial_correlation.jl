"""
    rdf(system; rmax[, nbins, partial, types])

Compute radial distribution function (RDF) over all atoms in `system`.

# Arguments
- `system::MDState`: the system for which the RDF is calculated

# Keywords
- `rmax::Real`: Radius up to which the RDF is calculated
- `nbins::Integer=250`: Number of bins in the RDF.
- `partial::Bool=false`: Whether to compute partial RDFs for types.
- `types::Union{Nothing,AbstractVector}=nothing`: Compute RDF using only particles with
    these types or use all particles if `types == nothing`.

# Returns
- `RDFMatrix{Float64}`: The computed RDF. `distance_range(gr)` returns the centers of the
    bins. `type_pairs(gr)` is an array of pairs `(i, j)` of types for which the RDF is
    computed. For non-partial RDF, `type_pairs(gr) == [(0, 0)]`.

# Throws
- `ArgumentError`: Thrown if `rmax` exceeds half of the `system`'s box size in any
    dimension.
"""
function rdf(
    system::MDState
    ;
    rmax::Real,
    nbins::Integer=250,
    partial::Bool=false,
    types::Union{Nothing,AbstractVector{<:Integer},Tuple}=nothing,
)
    return __rdf(system, rmax, nbins, partial, types)
end

function __rdf(
    system::MDState,
    rmax::Real,
    nbins::Integer,
    partial::Bool,
    types::Nothing,
)
    dr = float(rmax / nbins)
    if !partial
        typepairs = [(; types=(0, 0), fraction=1.0)]
        @inbounds gr = __rdf_impl!(
            zeros(Float64, nbins, 1), system, dr, rmax, types
        )
    else
        typefracs = Dict{Int,Int}()
        for t in system.type
            nt = get(typefracs, t, 0)
            typefracs[t] = nt + 1
        end
        typearray = sort!(collect(keys(typefracs)))
        ntotal = sum(values(typefracs))
        typepairs = map(
            (i, j) for i in eachindex(typearray) for j in i:lastindex(typearray)
        ) do (i, j)
            ti, tj = typearray[i], typearray[j]
            (
                ;
                types = (ti, tj),
                fraction = if ti == tj
                    (typefracs[ti] / ntotal)^2
                else
                    2 * (typefracs[ti] / ntotal) * (typefracs[tj] / ntotal)
                end
            )
        end
        @inbounds gr = __prdf_impl!(
            zeros(Float64, nbins, length(typepairs)), system, dr, rmax, typearray
        )
    end
    return KeyedArray(
        gr
        ;
        distance=dr/2:dr:rmax, types=typepairs
    )
end

function __rdf(
    system::MDState,
    rmax::Real,
    nbins::Integer,
    partial::Bool,
    types::Union{AbstractVector{<:Integer},Tuple},
)
    dr = float(rmax / nbins)
    if !partial
        typepairs = [(; types=(0, 0), fraction=1.0)]
    else
        typefracs = Dict{Int,Int}(t=>0 for t in types)
        for t in system.type
            nt = get(typefracs, t, nothing)
            if nt !== nothing
                typefracs[t] = nt + 1
            end
        end
        typearray = sort!(collect(keys(typefracs)))
        ntotal = sum(values(typefracs))
        typepairs = map(
            (i, j) for i in eachindex(typearray) for j in i:lastindex(typearray)
        ) do (i, j)
            ti, tj = typearray[i], typearray[j]
            (
                ;
                types = (ti, tj),
                fraction = if ti == tj
                    (typefracs[ti] / ntotal)^2
                else
                    2 * (typefracs[ti] / ntotal) * (typefracs[tj] / ntotal)
                end
            )
        end
    end
    @inbounds if partial
        gr = __prdf_impl!(
            zeros(Float64, nbins, length(typepairs)), system, dr, rmax, types
        )
    else
        gr = __rdf_impl!(
            zeros(Float64, nbins, 1), system, dr, rmax, types
        )
    end
    return KeyedArray(
        gr
        ;
        distance=dr/2:dr:rmax, types=typepairs
    )
end

Base.@propagate_inbounds function __rdf_impl!(
    gr::AbstractMatrix, system::MDState, dr::AbstractFloat, rmax::AbstractFloat, ::Nothing
)
    invdr = inv(dr)
    boxsize = diag(boxvectors(system))
    V = prod(boxsize)
    gr .= 0

    foreach(neighbor_pairs(system, rmax)) do pair
        slice_index = floor(Int, pbcdist(pair) * invdr) + 1
        gr[slice_index] += 1
    end
    ntotal = natoms(system)
    density = (ntotal - 1) / V
    vol_prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = vol_prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

Base.@propagate_inbounds function __rdf_impl!(
    gr::AbstractMatrix, system::MDState, dr::AbstractFloat, rmax::AbstractFloat, types
)
    invdr = inv(dr)
    boxsize = diag(boxvectors(system))
    V = prod(boxsize)
    gr .= 0

    type = system.type
    foreach(neighbor_pairs(system, rmax)) do pair
        i, j = atom_indices(pair)
        if (type[i] in types) && (type[j] in types)
            slice_index = floor(Int, pbcdist(pair) * invdr) + 1
            gr[slice_index] += 1
        end
    end
    ntotal = count(in(types), type)
    density = (ntotal - 1) / V
    vol_prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = vol_prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

Base.@propagate_inbounds function __prdf_impl!(
    gr::AbstractMatrix,
    system::MDState,
    dr::AbstractFloat,
    rmax::AbstractFloat,
    types::Union{AbstractArray{<:Integer},Tuple}
)
    ntypes = length(types)
    npart_types = zeros(Int, ntypes)
    invdr = inv(dr)
    boxvol = abs(det(boxvectors(system)))
    type = system.type
    for t in type
        t_ind = findfirst(==(t), types)
        t_ind === nothing && continue
        npart_types[t_ind] += 1
    end
    gr .= 0
    foreach(neighbor_pairs(system, rmax)) do pair
        (i, j) = atom_indices(pair)
        t1 = findfirst(==(type[i]), types)
        t2 = findfirst(==(type[j]), types)
        if (t1 === nothing) | (t2 === nothing)
            return nothing
        end
        t1, t2 = minmax(t1, t2)
        col = sum(ntypes:-1:ntypes-t1+2) + (t2 - t1 + 1)
        ibin = floor(Int, pbcdist(pair) * invdr) + 1
        gr[ibin, col] += 1
        return nothing
    end
    vol_prefactor = 4/3 * π * dr^3 / boxvol
    col_ind = 1
    for t_i in eachindex(types)
        for t_j in t_i:lastindex(types)
            if t_i == t_j
                npair_ref = div(npart_types[t_i] * (npart_types[t_i] - 1), 2)
            else
                npair_ref = npart_types[t_i] * npart_types[t_j]
            end
            if npair_ref > 0
                for i in axes(gr, 1)
                    prefactor = npair_ref * vol_prefactor
                    gr[i, col_ind] /= prefactor * (i^3 - (i-1)^3)
                end
            end
            col_ind += 1
        end
    end
    return gr
end

"""
    intra_rdf(system; rmax[, nbins, partial, types])

Compute intramolecular part of the radial distribution function (RDF) over all atoms in
    `system`.

# Arguments
- `system::MDState`: the system for which the RDF is calculated

# Keywords
- `rmax::Real`: Radius up to which the RDF is calculated
- `nbins::Integer=250`: Number of bins in the RDF.
- `partial::Bool=false`: Whether to compute partial RDFs for types.
- `types::Union{Nothing,AbstractVector,Tuple}=nothing`: Compute RDF using only particles
    with these types or use all particles if `types == nothing`.

# Returns
- `AxisMatrix{Float64}`: The computed RDF. `distance_range(gr)` returns the centers of the
    bins. `type_pairs(gr)` is an array of pairs `(i, j)` of types for which the RDF is
    computed. For all-to-all RDF, `type_pairs(gr) == [(0, 0)]`.

# Throws
- `ArgumentError`: Thrown if `rmax` exceeds half of the `system`'s box size in any
    dimension.
"""
function intra_rdf(
    system::MDState
    ;
    rmax::Real,
    nbins::Integer=250,
    partial=false,
    types::Union{Nothing,AbstractVector{<:Integer},Tuple}=nothing,
)
    dr = rmax / nbins
    if !partial
        typepairs = [(; types=(0, 0), fraction=1.0)]
    else
        if types === nothing
            typefracs = Dict{Int,Int}()
            for t in system.type
                nt = get(typefracs, t, 0)
                typefracs[t] = nt + 1
            end
        else
            typefracs = Dict{Int,Int}(t=>0 for t in types)
            for t in system.type
                nt = get(typefracs, t, nothing)
                if nt !== nothing
                    typefracs[t] = nt + 1
                end
            end
        end
        typearray = sort!(collect(keys(typefracs)))
        ntotal = sum(values(typefracs))
        typepairs = map(
            (i, j) for i in eachindex(typearray) for j in i:lastindex(typearray)
        ) do (i, j)
            ti, tj = typearray[i], typearray[j]
            (
                ;
                types = (ti, tj),
                fraction = if ti == tj
                    (typefracs[ti] / ntotal)^2
                else
                    2 * (typefracs[ti] / ntotal) * (typefracs[tj] / ntotal)
                end
            )
        end
    end
    @inbounds if partial
        gr = __intra_prdf_impl!(
            zeros(Float64, nbins, length(typepairs)), system, dr, rmax, typearray
        )
    else
        gr = __intra_rdf_impl!(
            zeros(Float64, nbins, length(typepairs)), system, dr, rmax, types
        )
    end
    return KeyedArray(
        gr
        ;
        distance=dr/2:dr:rmax, types=typepairs
    )
end

Base.@propagate_inbounds function __intra_prdf_impl!(
    gr::AbstractMatrix,
    system::MDState,
    dr::AbstractFloat,
    rmax::AbstractFloat,
    types::Union{AbstractArray{<:Integer},Tuple}
)
    ntypes = length(types)
    npart_types = zeros(Int, ntypes)
    invdr = inv(dr)
    boxvol = volume(system)
    type = system.type
    for t in type
        t_ind = findfirst(==(t), types)
        t_ind === nothing && continue
        npart_types[t_ind] += 1
    end
    gr .= 0
    for mol in molecules(system)
        for j in atom_indices(mol)
            tj = findfirst(==(type[j]), types)
            tj === nothing && continue
            for i in atom_indices(mol)
                i == j && break
                ti = findfirst(==(type[i]), types)
                ti === nothing && continue
                d = pbcdist(ParticlePair(system, i, j))
                d >= rmax && continue
                t1, t2 = minmax(ti, tj)
                col = sum(ntypes:-1:ntypes-t1+2) + (t2 - t1 + 1)
                ibin = floor(Int, d * invdr) + 1
                gr[ibin, col] += 1
            end
        end
    end
    vol_prefactor = 4/3 * π * dr^3 / boxvol
    col_ind = 1
    for t_i in eachindex(types)
        for t_j in t_i:lastindex(types)
            if t_i == t_j
                npair_ref = div(npart_types[t_i] * (npart_types[t_i] - 1), 2)
            else
                npair_ref = npart_types[t_i] * npart_types[t_j]
            end
            if npair_ref > 0
                for i in axes(gr, 1)
                    prefactor = npair_ref * vol_prefactor
                    gr[i, col_ind] /= prefactor * (i^3 - (i-1)^3)
                end
            end
            col_ind += 1
        end
    end
    return gr
end

Base.@propagate_inbounds function __intra_rdf_impl!(
    gr::AbstractMatrix,
    system::MDState,
    dr::AbstractFloat,
    rmax::AbstractFloat,
    types::Union{AbstractArray{<:Integer},Tuple}
)
    invdr = inv(dr)
    boxvol = volume(system)
    type = system.type
    ntotal = count(in(types), type)
    gr .= 0
    for mol in molecules(system)
        for j in atom_indices(mol)
            (type[j] in types) || continue
            for i in atom_indices(mol)
                i == j && break
                (type[i] in types) || continue
                d = pbcdist(ParticlePair(system, i, j))
                d >= rmax && continue
                ibin = floor(Int, d * invdr) + 1
                gr[ibin, 1] += 1
            end
        end
    end
    density = (ntotal - 1) / boxvol
    vol_prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = vol_prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

Base.@propagate_inbounds function __intra_rdf_impl!(
    gr::AbstractMatrix,
    system::MDState,
    dr::AbstractFloat,
    rmax::AbstractFloat,
    ::Nothing
)
    invdr = inv(dr)
    boxvol = volume(system)
    type = system.type
    ntotal = natoms(system)
    gr .= 0
    for mol in molecules(system)
        for j in atom_indices(mol)
            for i in atom_indices(mol)
                i == j && break
                d = pbcdist(ParticlePair(system, i, j))
                d >= rmax && continue
                ibin = floor(Int, d * invdr) + 1
                gr[ibin, 1] += 1
            end
        end
    end
    density = (ntotal - 1) / boxvol
    vol_prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = vol_prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

const RDFMatrix{T,R} = KeyedArray{
        T,
        2,
        NamedDimsArray{(:distance, :types),T,2,Matrix{T}},
        Tuple{R,Vector{NamedTuple{(:types, :fraction), Tuple{Tuple{Int, Int}, Float64}}}}
    }

"""
    rdf!(gr::RDFMatrix, system::MDState)

Compute radial distribution function (RDF) over all atoms in `system` and overwrite `gr`
    with it. The distance range and type pairs for computing the RDF are defined by
    [`distance_range(gr)`](@ref distance_range) and [`type_pairs(gr)`](@ref type_pairs).

# Arguments
- `gr::KeyedArray`: The array to write the RDF to.
- `system::MDState`: The system for which the RDF is calculated.

# Returns
- `RDFMatrix`: The array `gr` overwritten by the RDF.

See also: [`rdf`](@ref), [`intra_rdf`](@ref).
"""
function rdf!(gr::RDFMatrix, system::MDState)
    r_range = distance_range(gr)
    dr = step(r_range)
    rmax = last(r_range) + dr / 2
    typepairs = type_pairs(gr)
    @inbounds if length(typepairs) == 1 && typepairs[1] == (0, 0)
        __rdf_impl!(gr.data, system, dr, rmax, nothing)
    else
        __rdf_impl!(gr.data, system, dr, rmax, [i for (i, j) in typepairs if i==j])
    end
    return gr
end

"""
    distance_range(distcorr::KeyedArray)

Return the centers of bins in the `distcorr` (which must store a histogram of a
    distance-property correlation, such as RDF).
"""
distance_range(distcorr::KeyedArray) = distcorr.distance

"""
    type_pairs(gr::RDFMatrix)

Return the types over which the radial distribution function stored in `gr` is computed.
"""
type_pairs(gr::RDFMatrix) = mappedarray(k->k.types, gr.types)

"""
    reweigh(gr::RDFMatrix; weights)

Return the radial distribution function reweighted using weights for types. `weights[k]`
    is the weight of the `k`-th type.
"""
function reweigh(gr::RDFMatrix; weights)
    typepairs = type_pairs(gr)
    if length(typepairs) == 1 && typepairs[1] == (0, 0)
        return copy(gr)
    end
    total_weight = 0.0
    r_range = distance_range(gr)
    total_gr = zeros(Float64, (size(gr, 1), 1))
    for (pgr, (types, fraction)) in zip(eachcol(gr), axiskeys(gr, 2))
        t1, t2 = types
        new_weight = weights[t1] * weights[t2] * fraction
        total_gr .+= pgr * new_weight
        total_weight += new_weight
    end
    total_gr ./= total_weight
    return KeyedArray(total_gr; distance=r_range, types=[(; types=(0, 0), fraction=1.0)])
end

abstract type PairEntropyRDFReference end
struct SameTrajectoryRDFBias<:PairEntropyRDFReference end
struct NullRDFBias<:PairEntropyRDFReference end

struct PairEntropyCalculator{
    R<:Union{RDFMatrix,PairEntropyRDFReference},V<:Union{Nothing,AbstractVector}
}<:Function
    rdf_reference::R
    numeric_density_reference::Float64
    rmax::Float64
    nbins::Int
    partial::Bool
    types::V
end

"""
    pair_entropy_reftraj_bias(; filemask, timesteps, rdf_rmax, rdf_nbins, rdf_partial, types)

Prepare computation of pair entropy with the reference intramolecular RDF computed from an
existing trajectory.

# Keywords
- `filemask::AbstractString`: File mask for trajectory
- `timesteps::AbstractVector=0:typemax(Int)`: Use only specified timesteps for computation.
- `rdf_rmax::Real`: Radius up to which compute the RDF.
- `rdf_nbins::Integer=250`: Number of bins in the RDF.
- `rdf_partial::Bool=false`: Whether to compute partial RDFs for types.
- `types::Union{Nothing,AbstractVector,Tuple}=nothing`: Use RDF built with only specified
    types (`types = nothing` with `rdf_partial = true` means using all types,
    `types = nothing` with `rdf_partial = false` means treating all particles as the same
    type).

# Returns
A function which applies to an `MDState` and computes pair entropy with the correction based
    on the computed intramolecular RDF.
"""
function pair_entropy_reftraj_bias(
    ;
    filemask::AbstractString,
    timesteps::AbstractVector=0:typemax(Int),
    rdf_rmax::Real,
    rdf_nbins::Integer=250,
    rdf_partial::Bool=false,
    types::Union{Nothing,AbstractVector{<:Integer},Tuple}=nothing,
)
    ref_rdf = traj_average(filemask; timesteps=timesteps) do system
        intra_rdf(system; rmax=rdf_rmax, nbins=rdf_nbins, partial=rdf_partial, types=types)
    end

    if types === nothing
        ref_density = traj_average(filemask; timesteps=timesteps) do system
            natoms(system) / volume(system)
        end
    else
        ref_density = traj_average(filemask; timesteps=timesteps) do system
            count(in(types), system.type) / volume(system)
        end
    end
    ref_types = [i for (i, j) in type_pairs(ref_rdf) if i == j]
    return PairEntropyCalculator(
        ref_rdf, ref_density, rdf_rmax, rdf_nbins, rdf_partial, ref_types,
    )
end

function pair_entropy_null_bias(
    ;
    rdf_partial::Bool=false,
    rdf_rmax::Real,
    rdf_nbins::Integer=250,
    types=nothing
)
    return PairEntropyCalculator(
        NullRDFBias(),
        0.0,
        rdf_rmax,
        rdf_nbins,
        rdf_partial,
        types
    )
end

function pair_entropy_traj_bias(
    ;
    rdf_partial::Bool=false,
    rdf_rmax::Real,
    rdf_nbins::Integer=250,
    types=nothing
)
    return PairEntropyCalculator(
        SameTrajectoryRDFBias(),
        0.0,
        rdf_rmax,
        rdf_nbins,
        rdf_partial,
        types
    )
end

function pair_entropy_rdf_bias(; ref_rdf::RDFMatrix, ref_density::Real)
    types = [i for (i, j) in type_pairs(ref_rdf) if i == j]
    partial = length(types) == 1 && types[1] == 0
    r_range = distance_range(ref_rdf)
    nbins = length(r_range)
    rmax = last(r_range) + first(r_range)
    return PairEntropyCalculator(ref_rdf, ref_density, rmax, nbins, partial, types)
end

function pair_entropy(rdf::RDFMatrix, density::Real)
    rho = float(density)
    function integrand(gr)
        return gr * log(gr + 1e-100) - gr + 1.0
    end

    r_range = distance_range(rdf)
    s2_sum = zero(rho)
    for (prdf, (_, frac)) in zip(eachcol(rdf), axiskeys(rdf, 2))
        s2_sum += frac *
            sum(integrand(prdf[i]) * r_range[i]^2 for i in eachindex(r_range))
    end
    return -2 * pi * rho * s2_sum * step(r_range)
end

function ref_pair_entropy(rdf::RDFMatrix, density::Real, ref_density::Real=density)
    rho = float(density)
    rho_ratio = float(ref_density / density)
    function integrand(gr)
        return (gr + 1.0) * log(gr + 1.0) - gr
    end

    r_range = distance_range(rdf)
    s2_sum = zero(rho)
    for (prdf, (_, frac)) in zip(eachcol(rdf), axiskeys(rdf, 2))
        s2_sum += frac *
            sum(integrand(prdf[i] * rho_ratio) * r_range[i]^2 for i in eachindex(r_range))
    end
    return -2 * pi * rho * s2_sum * step(r_range)
end

function (s2::PairEntropyCalculator{RDFMatrix})(system::MDState)
    rmax = s2.rmax
    nbins = s2.nbins
    partial = s2.partial
    types = s2.types

    base_rdf = rdf(system; rmax, nbins, partial, types)

    density = count(in(types), system.type) / volume(system)

    ref_rdf = s2.rdf_reference
    ref_density = s2.numeric_density_reference

    base_s2 = pair_entropy(base_rdf, density)

    ref_s2 = ref_pair_entropy(ref_rdf, density, ref_density)

    return base_s2 - ref_s2
end

function (s2::PairEntropyCalculator{NullRDFBias})(system::MDState)
    rmax = s2.rmax
    nbins = s2.nbins
    partial = s2.partial
    types = s2.types

    base_rdf = rdf(system; rmax, nbins, partial, types)
    if types === nothing
        density = natoms(system) / volume(system)
    else
        density = count(in(types), system.type) / volume(system)
    end

    return pair_entropy(base_rdf, density)
end

function (s2::PairEntropyCalculator{SameTrajectoryRDFBias})(system::MDState)
    rmax = s2.rmax
    nbins = s2.nbins
    partial = s2.partial
    types = s2.types

    base_rdf = rdf(system; rmax, nbins, partial, types)
    ref_rdf = intra_rdf(system; rmax, nbins, partial, types)

    if types === nothing
        density = natoms(system) / volume(system)
    else
        density = count(in(types), system.type) / volume(system)
    end

    base_s2 = pair_entropy(base_rdf, density)

    ref_s2 = ref_pair_entropy(ref_rdf, density)

    return base_s2 - ref_s2
end

function traj_average(
    s2_calc::PairEntropyCalculator{<:RDFMatrix}, fmask::AbstractString,
    ;
    timesteps::AbstractVector=0:typemax(Int),
)
    rmax = s2_calc.rmax
    nbins = s2_calc.nbins
    partial = s2_calc.partial
    types = s2_calc.types

    base_rdf = traj_average(fmask; timesteps) do system
        rdf(system; rmax, nbins, partial, types)
    end

    density = traj_average(fmask; timesteps) do system
        count(in(types), system.type) / volume(system)
    end

    ref_rdf = s2_calc.rdf_reference
    ref_density = s2_calc.numeric_density_reference

    base_s2 = pair_entropy(base_rdf, density)

    ref_s2 = ref_pair_entropy(ref_rdf, density, ref_density)

    return base_s2 - ref_s2
end

function traj_average(
    s2_calc::PairEntropyCalculator{NullRDFBias}, fmask::AbstractString,
    ;
    timesteps::AbstractVector=0:typemax(Int),
)
    rmax = s2_calc.rmax
    nbins = s2_calc.nbins
    partial = s2_calc.partial
    types = s2_calc.types

    base_rdf = traj_average(fmask; timesteps) do system
        rdf(system; rmax, nbins, partial, types)
    end

    if types === nothing
        density = traj_average(fmask; timesteps=timesteps) do system
            natoms(system) / volume(system)
        end
    else
        density = traj_average(fmask; timesteps=timesteps) do system
            count(in(types), system.type) / volume(system)
        end
    end

    base_s2 = pair_entropy(base_rdf, density)

    return base_s2
end

function traj_average(
    s2_calc::PairEntropyCalculator{SameTrajectoryRDFBias}, fmask::AbstractString,
    ;
    timesteps::AbstractVector=0:typemax(Int),
)
    rmax = s2_calc.rmax
    nbins = s2_calc.nbins
    partial = s2_calc.partial
    types = s2_calc.types

    base_rdf = traj_average(fmask; timesteps) do system
        rdf(system; rmax, nbins, partial, types)
    end

    ref_rdf = traj_average(fmask; timesteps) do system
        intra_rdf(system; rmax, nbins, partial, types)
    end

    if types === nothing
        density = traj_average(fmask; timesteps=timesteps) do system
            natoms(system) / volume(system)
        end
    else
        density = traj_average(fmask; timesteps=timesteps) do system
            count(in(types), system.type) / volume(system)
        end
    end

    base_s2 = pair_entropy(base_rdf, density)
    ref_s2 = ref_pair_entropy(ref_rdf, density)

    return base_s2 - ref_s2
end

"""
    property_distance_correlation(system::MDState; rmax::Real, nbins::Integer, propertyname::Symbol)
    property_distance_correlation(system::MDState; rmax::Real, nbins::Integer, propertyvector::AbstractArray)

Compute the correlation ⟨*a*[*i*] ⋅ *a*[*j*]⟩(*r*) where *a* is the property which can be
    obtained from `system[propertyname]` or is given as `propertyvector`. `rmax` and `nbins`
    provide the maximum interparticle separation and number of histogram bins, respectively.
"""
function property_distance_correlation(
    system::MDState,
    ;
    rmax::Real,
    nbins::Integer,
    propertyname::Union{Nothing,Symbol}=nothing,
    propertyvector::Union{Nothing,AbstractVector}=nothing,
)
    if rmax >= minimum(diag(boxvectors(system)))
        throw(ArgumentError("Cut-off radius $rmax exceeds half of the simulation box dimension"))
    end
    if propertyname !== nothing && propertyvector !== nothing
        throw(ArgumentError("Either `propertyname` or `propertyvector` must be specified, but not both"))
    elseif propertyname !== nothing
        pvector = get(system, propertyname, nothing)
        if pvector === nothing
            throw(KeyError(propertyname))
        end
    elseif propertyvector !== nothing
        pvector = propertyvector
    else
        throw(ArgumentError("Either `propertyname` or `propertyvector` must be specified"))
    end

    return __property_distance_correlation(neighbor_pairs(system, rmax), rmax, nbins, pvector)
end

function __property_distance_correlation(
    pairs_max::NeighborPairs,
    rmax::Real,
    nbins::Integer,
    pvector::AbstractVector,
)
    dr = rmax / nbins

    stat = zeros(Float64, nbins, 3)

    foreach(pairs_max) do p
        dist = pbcdist(p)
        l, m = atom_indices(p)
        a_i = pvector[l]
        a_j = pvector[m]
        scalar_product = dot(a_i, a_j)
        s = floor(Int, dist / dr) + 1
        stat[s, 2] += scalar_product
        stat[s, 3] += 1
    end

    @views stat[:, 1] .= stat[:, 2] ./ (stat[:, 3] .+ 1e-100)

    return KeyedArray(
        stat
        ;
        distance=dr/2 : dr : rmax,
        data=[:average_correlation, :cumulative_correlation, :item_count]
    )
end
