"""
    read_dump!(file::Union{IO,AbstractString}, system::MDState; mode=:update)

Read state from `file` into `system`. With `mode=:update` (default), the fields that are
    available in the file are updated in `system`. With `mode=:replace`, `system` is fully
    cleared and the state is read afresh.
"""
function read_dump!(file::IO, system::MDState; mode=:update)
    mode in (:replace, :update) || error(
        "Invalid reading mode. Valid modes are `:update` and `:replace`."
    )
    if mode === :replace
        clear_state!(system)
    end
    a1 = readline(file)
    a1 == "ITEM: TIMESTEP" || error(
        "Invalid dump format: expected ITEM: TIMESTEP on line 1."
    )
    a = readline(file)
    t = tryparse(Int, a)
    if t === nothing
        error("Invalid dump format: expected timestep on line 2.")
    else
        system.timestep = t
    end

    a1 = readline(file)
    a1 == "ITEM: NUMBER OF ATOMS" || error(
        "Invalid dump format: expected ITEM: NUMBER OF ATOMS on line 3."
    )
    a = readline(file)
    n = tryparse(Int, a)
    n === nothing && error("Invalid dump format: expected number of atoms on line 4.")

    if mode === :update && n != natoms(system)
        error("Cannot update the state: number of atoms does not match. Use `mode=:replace` keyword to clear the initial state.")
    elseif mode === :replace
        system.nreal = n
    end

    a = readline(file)
    if (startswith(a, "ITEM: BOX BOUNDS"))
        atok = split(a)
        pbctype = (atok[end-2], atok[end-1], atok[end]) .== "pp"

        triclinic_box = length(atok) == 9
        ortho_box = length(atok) == 6

        if ortho_box
            system.box_type = ORTHO_CELL
        elseif triclinic_box
            system.box_type = TRICLINIC_CELL
        else
            error("Invalid dump format: cannot determine the box type.")
        end
        s1 = MVector(0.0, 0.0, 0.0)
        s2 = MVector(0.0, 0.0, 0.0)
        tilt = MVector(0.0, 0.0, 0.0)
        for i in 1:3
            a = readline(file)
            c = split(a)
            if triclinic_box
                s1[i], s2[i], tilt[i] = (tryparse(Float64, tok) for tok in c)
            else
                s1[i], s2[i] = (tryparse(Float64, tok) for tok in c)
            end
        end

        s1 .-= (
            min(0, tilt[1], tilt[2], tilt[1] + tilt[2]), min(0, tilt[3]), 0.0
        )
        s2 .-= (
            max(0, tilt[1], tilt[2], tilt[1] + tilt[2]), max(0, tilt[3]), 0.0
        )
        system.origin = s1
        system.box_vectors = SMatrix{3,3,Float64}(
            s2[1] - s1[1], 0, 0, tilt[1], s2[2] - s1[2], 0, tilt[2], tilt[3], s2[3] - s1[3]
        )
    else
        error("Invalid dump format: expected ITEM: BOX BOUNDS on line 5.")
    end
    a = readline(file)
    colnames = split(a)[3:end]

    table = (; map(s -> table_col(s), colnames)...)
    __fill_table!(file, table, n)

    __copytable!(system, table; mode=mode)
    system.pbc = pbctype
    return system
end

can_read_state(file) = !eof(file)

function __fill_table!(file::IO, table::NamedTuple, n::Integer)
    for _ in 1:n
        if eof(file)
            @warn "EOF while reading the dump file! Expect corrupted data"
            break
        end
        a = readline(file)
        tokens = split(a)
        if length(tokens) != length(table)
            @warn "EOF while reading the dump file! Expect corrupted data"
            break
        end
        parse_row!(table, tokens)
    end
end

function read_dump!(filepath::AbstractString, system::MDState; mode::Symbol=:update)
    open(filepath) do io
        read_dump!(io, system; mode=mode)
    end
end

"""
    read_dump(file::Union{IO,AbstractString})

Return an `MDState` read from a LAMMPS dump `file`.
"""
function read_dump(input::Union{IO,AbstractString})
    state = MDState()
    read_dump!(input, state; mode=:replace)
    return state
end

function __coord_type(table)
    count_s = count(haskey(table, p) for p in (:xs, :ys, :zs))
    count_w = count(haskey(table, p) for p in (:x, :y, :z))
    count_su = count(haskey(table, p) for p in (:xsu, :ysu, :zsu))
    count_u = count(haskey(table, p) for p in (:xu, :yu, :zu))

    count_coords, coord_type_ind = findmax((count_s, count_w, count_su, count_u))
    if count_coords < 3
        error("Cannot proceed with an incomplete coordinate set in atom properties $(propertynames(table))")
    end
    coord_type = (:scaled, :wrapped, :scaled_unwrapped, :unwrapped)[coord_type_ind]

    return coord_type
end

function __xcolumn(table, coord_type)
    if coord_type == :scaled
        return get(table, :xs, nothing)
    elseif coord_type == :wrapped
        return get(table, :x, nothing)
    elseif coord_type == :scaled_unwrapped
        return get(table, :xsu, nothing)
    elseif coord_type == :unwrapped
        return get(table, :xu, nothing)
    end
end

function __ycolumn(table, coord_type)
    if coord_type == :scaled
        return get(table, :ys, nothing)
    elseif coord_type == :wrapped
        return get(table, :y, nothing)
    elseif coord_type == :scaled_unwrapped
        return get(table, :ysu, nothing)
    elseif coord_type == :unwrapped
        return get(table, :yu, nothing)
    end
end

function __zcolumn(table, coord_type)
    if coord_type == :scaled
        return get(table, :zs, nothing)
    elseif coord_type == :wrapped
        return get(table, :z, nothing)
    elseif coord_type == :scaled_unwrapped
        return get(table, :zsu, nothing)
    elseif coord_type == :unwrapped
        return get(table, :zu, nothing)
    end
end

function __copytable!(system::MDState, table::NamedTuple; mode::Symbol)
    n = length(first(table))

    # mode is one of (:update, :replace)
    # with mode = :update, we assume that the length of the table is already equal to the
    # number of atoms in the system

    coord, vel = system.coord, system.vel

    if mode === :replace
        resize!(system._image, n)
        resize!(system.coord, n)
        resize!(system.vel, n)
        resize!(system.id, n)
        resize!(system.type, n)
        resize!(system.mol, n)
        resize!(system._element, n)
        resize!(system._mass, n)
        system._hasfields = 0x0000
        fill!(vel, zero(eltype(vel)))
    end

    if haskey(table, :id)
        sortperm!(system.id, table.id)
    else
        system.id .= 1:n
    end

    # By default:
    # - each particle is a separate molecule
    # - all particles are of type 1
    # - all particles have mass 1.0
    # - element names are empty
    if mode == :replace
        if !haskey(table, :mol)
            system.mol .= 1:n
        end

        if !haskey(table, :element)
            system._element .= ""
        end

        if !haskey(table, :mass)
            system._mass .= 1.0
        end

        if !haskey(table, :type)
            if haskey(table, :element)
                eltnames = sort!(unique(table.element))
                for i in eachindex(system.type)
                    j = system.id[i]
                    itype = searchsortedfirst(eltnames, table.element[j])
                    system.type[i] = itype
                end
            elseif haskey(table, :mass)
                atommasses = sort!(unique(table.mass))
                for i in eachindex(system.type)
                    j = system.id[i]
                    itype = searchsortedfirst(atommasses, table.mass[j])
                    system.type[i] = itype
                end
            else
                system.type .= 1
            end
        end
    end

    coord_type = __coord_type(table)

    xcolumn = __xcolumn(table, coord_type)
    ycolumn = __ycolumn(table, coord_type)
    zcolumn = __zcolumn(table, coord_type)

    scale = boxvectors(system)
    invscale = inv(scale)

    image_column_count = count(haskey(table, p) for p in (:ix, :iy, :iz))

    coords_are_wrapped = coord_type in (:scaled, :wrapped)
    has_images = image_column_count > 0
    all_images = image_column_count == 3 || coord_type in (:unwrapped, :scaled_unwrapped)

    if coords_are_wrapped && has_images && !all_images
        @warn "Ignoring an incomplete set of image flags"
    elseif image_column_count == 3
        ixcolumn = table.ix
        iycolumn = table.iy
        izcolumn = table.iz
    end

    if all_images
        system._hasfields |= HASIMAGES_MASK
    else
        system._hasfields &= ~HASIMAGES_MASK
    end

    fill!(system._image, Int16.((0, 0, 0)))

    vecfv = reinterpret(Float64, vel)
    xvecfv = @view vecfv[1:3:end]
    yvecfv = @view vecfv[2:3:end]
    zvecfv = @view vecfv[3:3:end]

    for (name, col) in pairs(table)
        if name === :vx
            system._hasfields |= HASVX_MASK
            xvecfv .= @view col[system.id]
        elseif name === :vy
            system._hasfields |= HASVY_MASK
            yvecfv .= @view col[system.id]
        elseif name === :vz
            system._hasfields |= HASVZ_MASK
            zvecfv .= @view col[system.id]
        elseif name === :mol
            system.mol .= @view col[system.id]
        elseif name === :type
            system.type .= @view col[system.id]
        elseif name === :mass
            system._hasfields |= HASMASS_MASK
            system._mass .= @view col[system.id]
        elseif name === :element
            system._hasfields |= HASELEMENT_MASK
            system._element .= @view col[system.id]
        elseif name in (:ix, :iy, :iz)
            continue
        elseif col === xcolumn
            continue
        elseif col === ycolumn
            continue
        elseif col === zcolumn
            continue
        elseif name !== :id
            if mode === :replace || !haskey(system._scalars, name)
                addproperty!(system; name, data=@view(col[system.id]), allow_sharing=true)
            else
                system._scalars[name] .= @view(col[system.id])
            end
        end
    end

    if coord_type == :scaled
        for i in eachindex(coord)
            j = system.id[i]
            rs = SVector(xcolumn[j], ycolumn[j], zcolumn[j])
            coord[i] = scale * mod.(rs, 1) + boxorigin(system)
        end
        if all_images
            for i in eachindex(system.image)
                j = system.id[i]
                rs = SVector(xcolumn[j], ycolumn[j], zcolumn[j])
                system._image[i] = floor.(rs) .+ (ixcolumn[j], iycolumn[j], izcolumn[j])
            end
        end
    elseif coord_type == :wrapped
        for i in eachindex(coord)
            j = system.id[i]
            rw = SVector(xcolumn[j], ycolumn[j], zcolumn[j])
            rr = rw - boxorigin(system)
            rs = invscale * rr
            i0 = floor.(rs)
            coord[i] = rw - scale * i0
            if all_images
                system._image[i] = i0 .+ (ixcolumn[j], iycolumn[j], izcolumn[j])
            end
        end
    elseif coord_type == :scaled_unwrapped
        for i in eachindex(coord, xcolumn, ycolumn, zcolumn)
            j = system.id[i]
            rs = SVector(xcolumn[j], ycolumn[j], zcolumn[j])
            coord[i] = scale * mod.(rs, 1) + boxorigin(system)
            system._image[i] = floor.(rs)
        end
    elseif coord_type == :unwrapped
        for i in eachindex(coord, xcolumn, ycolumn, zcolumn)
            j = system.id[i]
            rw = SVector(xcolumn[j], ycolumn[j], zcolumn[j])
            rr = rw - boxorigin(system)
            rs = invscale * rr
            i0 = floor.(rs)
            coord[i] = rw - scale * i0
            system._image[i] = i0
        end
    end

    if haskey(table, :id)
        system.id .= table.id
        sort!(system.id)
    end

    return system
end

"""
    table_col(s)

Return a pair `Symbol => Vector` with the `eltype` of vector corresponding to name `s`.
"""
function table_col(s)
    if s in ("id", "mol", "type")
        return Symbol(s) => Int[]
    elseif s in ("element",)
        return Symbol(s) => String[]
    else
        return Symbol(s) => Float64[]
    end
end

"""
    parse_row!(table, tokens)

Parse `tokens` and push the values into the corresponding column in `table`.
"""
function parse_row!(table, tokens)
    foreach(table, tokens) do col, token
        if eltype(col) <: Real
            push!(col, parse(eltype(col), token))
        else
            push!(col, token)
        end
    end
    return
end

function update_coord!(system::MDState, newcoord)
    nextra = length(newcoord) - natoms(system)
    if nextra > 0
        append!(system._image, Iterators.repeated(zero(eltype(system._image)), nextra))
        append!(system.vel, Iterators.repeated((0.0, 0.0, 0.0), nextra))
        append!(system.type, Iterators.repeated(1, nextra))
        append!(system._mass, Iterators.repeated(1, nextra))
        append!(system._element, Iterators.repeated("", nextra))
        append!(system.id, range(get(system.id, length(system.id), 0)+1, length=nextra))
        append!(system.mol, range(get(system.mol, length(system.mol), 0)+1, length=nextra))
    end
    resize!(system.coord, length(newcoord))
    for (i, coord) in enumerate(newcoord)
        offset_coord = coord - system.origin
        coord = system.origin + mod.(offset_coord, diag(boxvectors(system)))
        image = floor.(Int16, offset_coord ./ diag(boxvectors(system)))
        system.coord[i] = coord
        system._image[i] = image
    end
    system.nreal = length(newcoord)
    return system
end
