"""
    __displacements(state::MDState, ref_state::MDState)

Return displacement vectors `rᵢ(state) - rᵢ(ref_state)` for all particles.
"""
function __displacements(state::MDState, ref_state::MDState)
    if id_tags(state) != id_tags(ref_state)
        error("Cannot compute displacements: IDs do not coincide")
    end

    if iswrapped(state) || iswrapped(ref_state)
        let
            boxvecs = boxvectors(state)
            pbc = pbcflags(state)
            coord = wrapped_positions(state)
            ref_coord = wrapped_positions(ref_state)
            displacement = pbcvector.(ref_coord, coord, Ref(diag(boxvecs)), Ref(pbc))
            return displacement
        end
    else
        let
            coord = unwrapped_positions(state)
            ref_coord = unwrapped_positions(ref_state)
            displacement = coord .- ref_coord
            return displacement
        end
    end
end

"""
    displacements(state::MDState; from::MDState)

Return a vector of displacement vectors `rᵢ(state) - rᵢ(from)` for all particles.
"""
displacements(state::MDState; from::MDState) = __displacements(state, from)

"""
    msd(state::MDState; from::MDState)

Compute the mean square displacement of particles in `state` relative to `from`.
"""
msd(state::MDState; from::MDState) = __msd(state, from)

function __msd(state::MDState, ref_state::MDState)
    if id_tags(state) != id_tags(ref_state)
        error("Cannot compute MSD: IDs do not coincide")
    end

    if iswrapped(state) || iswrapped(ref_state)
        let
            boxvecs = boxvectors(state)
            pbc = pbcflags(state)
            coord = wrapped_positions(state)
            ref_coord = wrapped_positions(ref_state)
            displacement = mappedarray(coord, ref_coord) do r, rref
                pbcvector(rref, r, diag(boxvecs), pbc)
            end
            return sum(mag2, displacement) / natoms(state)
        end
    else
        let
            coord = unwrapped_positions(state)
            ref_coord = unwrapped_positions(ref_state)
            displacement = mappedarray(coord, ref_coord) do r, rref
                r - rref
            end
            return sum(mag2, displacement) / natoms(state)
        end
    end
end

"""
    vel_distribution!(dist, system, vmin, vmax, dv)

Compute distribution of velocity components in the range from `vmin` to `vmax`
with the step `dv` and overwrite `dist` with it.
"""
# function vel_distribution!(
#     dist::AbstractVector, system::MDState, vmin, vmax, dv
# )
#     vel = system.vel
#     dist .= 0
#     for iatom in 1:length(vel), i in 1:3
#         nbin = floor(Int, (vel[iatom][i] - vmin) / dv) + 1
#         if nbin in eachindex(dist)
#             dist[nbin] += 1
#         end
#     end
#     dist ./= sum(dist)
#     return dist
# end

"""
    vel_distribution(system, vmin, vmax, dv)

Compute distribution of velocity components in the range from `vmin` to `vmax`
with the step `dv`.
"""
# function vel_distribution(system::MDState, vmin, vmax, dv)
#     n_distr_bins = ceil(Int, (vmax - vmin) / dv)
#     distr = zeros(Float64, n_distr_bins)
#     return vel_distribution!(distr, system, vmin, vmax, dv)
# end

# function coordination(system::MDState, rmax::Real)
#     build_cell_list!(system, rmax)
#     coord = system.coord
#     cell_list = system.cell_list
#     success_checks = 0
#     fail_checks = 0
#     boxsize = diag(boxvectors(system))
#     V = prod(boxsize)
#     for i in CartesianIndices(cell_list)
#         isempty(cell_list[i]) && continue
#         neigh_i = neigh_atoms(system, i)
#         for atom1 in neigh_i
#             if atom1 > natoms(system) # system.nreal
#                 break
#             end
#             @inbounds r1 = coord[atom1]
#             for atom2 in cell_list[i]
#                 atom2 < atom1 || break
#                 @inbounds r2 = coord[atom2]
#                 Δr12sq = pbcdist2(r1, r2, boxsize)
#                 if Δr12sq < rmax^2
#                     success_checks += 1
#                 else
#                     fail_checks += 1
#                 end
#             end
#         end
#     end
#     ntotal = natoms(system)
#     return (
#         coordination = success_checks / ntotal,
#         fails = fail_checks / ntotal,
#         particles_per_cell = ntotal / prod(size(system.cell_list))
#     )
# end

"""
    fourpoint_cc(system, systemnext, rbracket)

Compute the correlation coefficient between displacements of atom pairs in `system` and
    `systemnext` that are within `rbracket` apart in `system`.

# Arguments
- `system::MDState`: the base system
- `systemnext::MDState`: the system with displaced atoms
- `rbracket::Tuple`: the distance bracket in the form `(rmin, rmax)`

# Returns
- `Float64`: the correlation coefficient
"""
# function fourpoint_cc(
#     system::MDState,
#     systemnext::MDState,
#     rbracket::Tuple{Real,Real},
# )
#     rlo, rhi = rbracket
#     build_cell_list!(system, rhi)
#     coord = system.coord
#     coordnext = systemnext.coord
#     dcoord = coordnext .- coord
#     cell_list = system.cell_list
#     boxsize = diag(system.box_vectors)
#     accum = 0.0
#     pair_count = 0
#     for idx in CartesianIndices(cell_list)
#         neigh_i = neigh_atoms(system, idx)
#         for i in neigh_i
#             r1 = coord[i]
#             d1 = dcoord[i]
#             for j in cell_list[idx]
#                 j < i || break
#                 r2 = coord[j]
#                 d2 = dcoord[j]
#                 Δr12 = pbcdist(r1, r2, boxsize)
#                 if rlo < Δr12 < rhi
#                     accum += coss(d1, d2)
#                     pair_count += 1
#                 end
#             end
#         end
#     end
#     return pair_count == 0 ? 0.0 : accum / pair_count
# end


"""
    fourpoint_cc(system, systemnext; r₀, thickness)

Compute the correlation coefficient between displacements of atom pairs in `system` and
    `systemnext` for which the distances are in the interval
    `(r₀ - thickness/2, r₀ + thickness/2)`.

# Arguments
- `system::MDState`: the base system
- `systemnext::MDState`: the system with displaced atoms

# Keywords
- `r₀`: the median radius of the spherical layer
- `thickness`: the thickness of the spherical layer

# Returns
- `Float64`: the correlation coefficient
"""
# function fourpoint_cc(
#     system::MDState,
#     systemnext::MDState;
#     r₀::Real,
#     thickness::Real,
# )
#     rbracket = (r₀ - thickness / 2, r₀ + thickness / 2)
#     return fourpoint_cc(system, systemnext, rbracket)
# end

# function fourpoint_cc_rsweep!(
#     accum::AbstractVector,
#     lengths::AbstractVector,
#     system::MDState,
#     systemnext::MDState;
#     rmax::Real,
#     nbin::Real,
# )
#     build_cell_list!(system, rmax)
#     coord = system.coord
#     coordnext = systemnext.coord
#     cell_list = system.cell_list
#     boxsize = system.size
#     dr = rmax / nbin
#     for idx in CartesianIndices(cell_list)
#         neigh_i = neigh_atoms(system, idx)
#         for i in neigh_i
#             r1 = coord[i]
#             d1 = coordnext[i] - r1
#             for j in cell_list[idx]
#                 j < i || break
#                 r2 = coord[j]
#                 Δr12 = pbcdist(r1, r2, boxsize)
#                 if Δr12 < rmax
#                     d2 = coordnext[j] - r2
#                     slice_index = floor(Int, Δr12 / dr) + 1
#                     accum[slice_index] += coss(d1, d2)
#                     lengths[slice_index] += 1
#                 end
#             end
#         end
#     end
#     map!(accum, accum, lengths) do (acc, n)
#         ave = acc / n
#         return isnan(ave) ? zero(ave) : ave
#     end
#     return accum
# end

# function fourpoint_cc_rsweep(
#     system::MDState{T},
#     systemnext::MDState{T};
#     rmax::Real,
#     nbin::Real,
# ) where {T}
#     accum = similar(system.coord, T)
#     lengths = similar(accum, Int)
#     return fourpoint_cc_rsweep!(accum, lengths, system, systemnext; rmax, nbin)
# end

"""
    mol_com(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for all molecules in `system` and having the center-of-mass
    velocities of molecules.
"""
function mol_com(system::MDState)
    return map(_->NamedTuple(), molecules(system))
end

"""
    molecule_planes(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for molecules in `system`. Particles get a vector property
    `:normal` which is the normal vector for a best-fit plane drawn through the atoms of
    each molecule (effectively, the axis of the inertia tensor with the largest moment of
    inertia).

See also: [`inertia_tensor`](@ref), [`gyration_tensor`](@ref).
"""
function molecule_planes(system::MDState)
    mols = molecules(system)
    buf = map(mols) do mol
        return (; normal=fit_normal(mol))
    end
    return buf
end

"""
    molecule_planes(mols::Molecules)

Return an `MDState` with the same box bounds as `mols`' parent state with particles located
    at the center-of-mass positions for molecules in `system`. Particles get a vector
    property `:normal` which is the normal vector for a best-fit plane drawn through the
    atoms of each molecule (effectively, the axis of the inertia tensor with the largest
    moment of inertia).

See also: [`inertia_tensor`](@ref), [`gyration_tensor`](@ref), [`fit_normal`](@ref).
"""
function molecule_planes(mols::Molecules)
    buf = map(mols) do mol
        return (; normal=fit_normal(mol))
    end
    return buf
end

function molecule_planes(dump_file::AbstractString)
    system = read_dump(dump_file)
    return molecule_planes(system)
end

"""
    molecule_lines(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for molecules in `system`. Particles get a vector property
    `:direction` which is the result of `fit_line` for each molecule.

See also: [`fit_line`](@ref)
"""
function molecule_lines(system::MDState)
    mols = molecules(system)
    buf = map(mols) do mol
        return (; direction=fit_line(mol))
    end
    return buf
end

"""
    msdt(dir::AbstractString, mask::AbstractString, dt::Integer)

Compute the average molecular MSD for all dumps in `dir` that match `mask` and are `dt` time steps apart.
`mask` has the form `file.*.extension` where `*` is the timestep number
(following the LAMMPS naming convention).
"""
# function msdt(mask::AbstractString, dt::Integer)
#     return traj_correlate(mask, dt) do system, systemref
#         molec_com = mol_com(system)
#         molec_com_ref = mol_com(systemref)
#         return msd(molec_com, molec_com_ref)
#     end
# end

"""
    correlate_normals(mask::AbstractString, dt::Integer)

Return the trajectory-averaged value of `⟨n(t)⋅n(t + dt)⟩` where `n` are normal vectors to
    the molecular planes, angle brackets denote the average over all molecules in the
    system.
"""
function correlate_normals(fmask::AbstractString, dt::Integer)
    return traj_correlate(fmask; step_delta=dt) do system, systemref
        buf = molecule_planes(system)
        buf_ref = molecule_planes(systemref)
        dot_buf = dot.(buf.normal, buf_ref.normal)
        return sum(dot_buf) / length(dot_buf)
    end
end
