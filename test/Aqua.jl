using Aqua
using StatsBase

@testset "Aqua.jl" begin
    Aqua.test_ambiguities(
        MDProcessing;
        exclude = [
            ==,
            reshape,
            similar,
            vcat,
            Base.Broadcast.BroadcastStyle,
            StatsBase.TestStat
        ]
    )
    Aqua.test_unbound_args(MDProcessing)
    Aqua.test_undefined_exports(MDProcessing)
    Aqua.test_project_extras(MDProcessing)
    Aqua.test_stale_deps(MDProcessing)
    Aqua.test_deps_compat(MDProcessing)
    Aqua.test_piracies(MDProcessing)
end
