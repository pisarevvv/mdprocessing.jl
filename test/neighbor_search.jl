@testset "Neighbor search" begin
    system = read_dump("./data/lj.T1.d0.7.0.dump")
    pairs = [
        MDProcessing.ParticlePair(system, i, j)
        for i in 1:natoms(system) for j in 1:i-1
    ]

    for rcut in 1.5:0.5:5.5
        pairs_ref = sort!([atom_indices(p) for p in pairs if pbcdist(p) < rcut])
        pairs_our = sort!(atom_indices.(collect(neighbor_pairs(system, rcut))))
        @test pairs_ref == pairs_our
    end

    @test begin
        pairs_ref = [p for p in pairs if pbcdist(p) < 2.5]
        neigh_pairs = neighbor_pairs(system, 2.5)
        all(pairs_ref) do pair
            pair in neigh_pairs
        end
    end

    @test begin
        pairs_ref = [p for p in pairs if pbcdist(p) >= 2.5]
        neigh_pairs = neighbor_pairs(system, 2.5)
        all(pairs_ref) do pair
            !(pair in neigh_pairs)
        end
    end

    @test begin
        pairs_ref = [p for p in pairs if pbcdist(p) < 2.5]
        neigh_pairs = filter(p->pbcdist(p)<2.5, neighbor_pairs(system, 3.0))
        all(pairs_ref) do pair
            pair in neigh_pairs
        end
    end

    @test begin
        pairs_ref = [p for p in pairs if pbcdist(p) >= 2.5]
        neigh_pairs = filter(p->pbcdist(p)<2.5, neighbor_pairs(system, 3.0))
        all(pairs_ref) do pair
            !(pair in neigh_pairs)
        end
    end

    @test begin
        neigh_1 = neighbor_pairs(system, 3.0)
        neigh_2 = filter(neighbor_pairs(system; rcut=3.0)) do pp
            i, j = pp
            return i > j
        end

        collect(neigh_1) == collect(neigh_2)
    end

    @test begin
        neigh_1 = [pp for pp in neighbor_pairs(system, 3.0) if pbcdist(pp) >= 2.0]
        neigh_2 = filter(neighbor_pairs(system; rcut=3.0)) do pp
            return pbcdist(pp) >= 2.0
        end

        neigh_1 == collect(neigh_2)
    end

    @test begin
        neigh_1 = [pp for pp in neighbor_pairs(system, 3.0) if pbcdist(pp) >= 2.0]
        neigh_2 = filter(neighbor_pairs(system; rcut=3.0)) do pp
            return pbcdist(pp) >= 2.0
        end
        neigh_3 = filter(neigh_2) do pp
            return pbcdist(pp) >= 1.5
        end

        neigh_1 == collect(neigh_3)
    end

    @test begin
        neigh_count_1 = count(neighbor_pairs(system; rcut=3.0)) do pp
            return pbcdist(pp) < 3.0
        end

        neigh_count_2 = count(p->true, neighbor_pairs(system; rcut=3.0))

        neigh_count_3 = reduce(neighbor_pairs(system; rcut=3.0); init=0) do accum, pp
            return accum + 1
        end

        neigh_count_1 == neigh_count_2 == neigh_count_3
    end

    @test_throws MethodError reduce(neighbor_pairs(system; rcut=0.5)) do accum, pp
        return accum + 1
    end
end
