@testset "Collection interface" begin
    clay_pentane = read_dump("./data/C5H12.clay.1000.dump")

    pentane = filter(p -> p[:type] > 13, clay_pentane)

    @test all(MDProcessing.GLOBAL_DATA_FIELDS) do p
        getfield(pentane, p) == getfield(clay_pentane, p)
    end

    @test begin
        rem(natoms(pentane), 17) == 0
    end

    @test begin
        sum(unwrapped_positions(pentane)) isa AbstractVector
    end

    @test first(pentane) isa MDProcessing.MDParticle
end
