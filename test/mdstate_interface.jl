@testset "Basic properties" begin
    state = read_dump("./data/lj.T1.d0.6.0.dump")
    @test natoms(state) == 1000
    @test hasvelocity(state) == (false, false, false)
    @test !iswrapped(state)
    @test !has_element_name(state)
    @test !hasmass(state)
    @test state[:element] === missing
    @test state[:mass] === missing

    c11h10 = read_dump("data/323.0.995.0.dump")

    @test natoms(c11h10) == 512 * 21
    @test hasvelocity(c11h10) == (true, true, true)

    c6h6 = read_dump("data/benzene_wrapped.dump")
    @test hasmass(c6h6)
    @test iswrapped(c6h6)
    @test c6h6[:mass] isa Vector{Float64}
end

@testset "Adding properties" begin
    state = read_dump("./data/lj.T1.d0.6.0.dump")
    # adding a scalar property
    scalar_data = ones(Float64, natoms(state))
    @test addproperty!(state; name="scalar_shared", data=scalar_data, allow_sharing=true) isa Any
    @test state[:scalar_shared] === scalar_data
    @test addproperty!(state; name="scalar_copy", data=scalar_data, allow_sharing=false) isa Any
    @test state[:scalar_copy] !== scalar_data

    # adding a vector property
    vector_data = [zeros(3) for _ in state.coord]
    matrix_data = zeros(3, natoms(state))
    @test addproperty!(state; name="vec_from_vec", data=vector_data) isa Any
    @test addproperty!(state; name="vec_from_matr", data=matrix_data) isa Any


    bad_scalars = ones(1)
    bad_matrix = zeros(4, natoms(state))
    # adding a property that is already present must fail
    @test_throws ArgumentError addproperty!(state; name="coord", data=scalar_data) isa Any
    # adding a property with wrong dimensions must fail
    @test_throws DimensionMismatch addproperty!(state; name="bad_scalars", data=bad_scalars)
    @test_throws DimensionMismatch addproperty!(state; name="bad_vectors", data=bad_matrix)
end

@testset "Copying and clearing state" begin
    state = read_dump("./data/lj.T1.d0.6.0.dump")
    copy_state = deepcopy(state)

    @test all(
        state[p] == copy_state[p]
        for p in keys(state)
    )

    @test Set(keys(copy_state)) == Set(keys(state))

    clear_state!(copy_state)
    @test all(
        p -> isempty(copy_state[p]),
        keys(copy_state)
    )
end

@testset "Setting element names and masses" begin
    base = read_dump("data/1,1-DPE.00000")

    copy1 = deepcopy(base)
    @test !hasmass(copy1)
    set_typemasses!(copy1, (12.011, 12.011, 12.011, 12.011, 1.008, 1.008))

    @test hasmass(copy1)

    copy2 = deepcopy(base)
    @test !has_element_name(copy2)
    set_typenames!(copy2, ("C", "C", "C", "C", "H", "H"))
    @test has_element_name(copy2)
    @test element_names(copy2) ==
        repeat(
            [repeat(["C"], 14); repeat(["H"], 14)], div(natoms(base), 28)
        )

    @test !hasmass(copy2)
    set_elementmasses!(copy2, (C=12.011, H=1.008))
    @test masses(copy2) == masses(copy1)


    @test_throws ArgumentError set_typemasses!(base, (12.011,))
    @test_throws ArgumentError set_typenames!(base, ("C",))
    @test_throws ArgumentError set_elementmasses!(base, (C=12.011, H=1.008))
end

@testset "Printing" begin
    base = read_dump("data/1,1-DPE.00000")

    @test string(base) isa String

    @test string(first(base)) isa String
end
