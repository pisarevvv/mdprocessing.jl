@testset "Displacement" begin
    mag2(vec) = vec' * vec
    state = read_dump("data/1,1-DPE.01000")
    ref_state = read_dump("data/1,1-DPE.00000")

    @test displacements(state; from=ref_state) ==
        unwrapped_positions(state) - unwrapped_positions(ref_state)

    @test msd(state; from=ref_state) ==
        sum(mag2, unwrapped_positions(state) - unwrapped_positions(ref_state)) / natoms(state)

    copy_state = deepcopy(state)
    copy_state._hasfields &= ~MDProcessing.HASIMAGES_MASK
    fill!(copy_state._image, (0, 0, 0))

    @test isapprox(displacements(copy_state; from=ref_state),
        unwrapped_positions(state) - unwrapped_positions(ref_state),
        ;
        atol=1e-10
    )

    @test !isapprox(displacements(copy_state; from=ref_state),
        unwrapped_positions(copy_state) - unwrapped_positions(ref_state),
        ;
        atol=1e-10
    )

    @test isapprox(msd(copy_state; from=ref_state),
        sum(mag2, unwrapped_positions(state) - unwrapped_positions(ref_state)) / natoms(state),
        ;
        atol=1e-10
    )
end
