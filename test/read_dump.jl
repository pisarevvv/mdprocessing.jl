@testset "Reading dump files" begin
    @test read_dump("./data/lj.T1.d0.6.0.dump") isa MDState

    state_fresh = read_dump("./data/lj.T1.d0.6.0.dump")
    state_overwrite = let state = read_dump("./data/lj.T1.d0.7.0.dump")
        read_dump!("./data/lj.T1.d0.6.0.dump", state; mode=:replace)
        state
    end
    @test all(propertynames(state_fresh)) do p
        getproperty(state_fresh, p) == getproperty(state_overwrite, p)
    end

    @test Set(keys(state_fresh)) == Set([:id, :type, :mol, :coord, :vel, :c_epot, :c_ekin])

    @test Set(keys(state_fresh)) == Set(keys(state_overwrite))

    @test all(
        get(state_fresh, p, nothing) isa Vector
        for p in keys(state_fresh)
    )

    @test state_fresh[:fx] === missing

    @test get(state_fresh, :fx, nothing) === nothing

    @test all(
        length(state_fresh[p]) == length(state_fresh.coord)
        for p in keys(state_fresh)
    )

    @test all(
        length(state_overwrite[p]) == length(state_overwrite.coord)
        for p in keys(state_overwrite)
    )

    @test natoms(state_fresh) / det(boxvectors(state_fresh)) ≈ 0.6

    benzene_w = read_dump("data/benzene_wrapped.dump")
    benzene_u = read_dump("data/benzene_unwrapped.dump")
    benzene_sw = read_dump("data/benzene_wrapped_scaled.dump")
    benzene_su = read_dump("data/benzene_unwrapped_scaled.dump")

    @test iswrapped(benzene_w)

    @test !iswrapped(benzene_u)

    @test wrapped_positions(benzene_w) ≈ wrapped_positions(benzene_u)
    @test wrapped_positions(benzene_w) ≈ wrapped_positions(benzene_sw)
    @test unwrapped_positions(benzene_u) ≈ unwrapped_positions(benzene_su)
end

@testset "In-place overwrite dump files" begin
    state_fresh = read_dump("./data/1,1-DPE.01000")
    state_overwrite = let state = read_dump("./data/1,1-DPE.00000")
        read_dump!("./data/1,1-DPE.01000", state; mode=:update)
        state
    end
    @test all(propertynames(state_fresh)) do p
        getproperty(state_fresh, p) == getproperty(state_overwrite, p)
    end

    @test Set(keys(state_fresh)) == Set(keys(state_overwrite))

    @test all(
        haskey(state_fresh, p)
        for p in (:id, :type, :mol, :coord)
    )

    @test all(
        state_fresh[p] == state_overwrite[p]
        for p in keys(state_overwrite)
    )

    @test let
        state_lj_fresh = read_dump("./data/lj.T1.d0.7.0.dump")
        state_lj_overwrite = let state = read_dump("./data/lj.T1.d0.6.0.base.dump")
            read_dump!("./data/lj.T1.d0.7.0.dump", state; mode=:update)
            state
        end
        all(keys(state_lj_overwrite)) do p
            state_lj_fresh[p] == state_lj_overwrite[p]
        end
    end

    @test let
        state_lj_fresh = read_dump("./data/lj.T1.d0.7.0.dump")
        state_lj_overwrite = let state = read_dump("./data/lj.T1.d0.6.0.dump")
            read_dump!("./data/lj.T1.d0.7.0.dump", state; mode=:update)
            state
        end
        all(keys(state_lj_overwrite)) do p
            state_lj_fresh[p] == state_lj_overwrite[p]
        end
    end
end

@testset "Nonzero origin" begin
    state_c6h6 = read_dump("./data/benzene_wrapped.dump")
    vecs = boxvectors(state_c6h6)
    r0 = boxorigin(state_c6h6)

    @test boxorigin(state_c6h6) == SVector(-24.0, -24.0, -24.0)

    @test boxvectors(state_c6h6) == @SMatrix [
        48.0  0.0  0.0
         0.0 48.0  0.0
         0.0  0.0 48.0
    ]

    @test all(
        zip(
            wrapped_positions(state_c6h6),
            state_c6h6._image,
            unwrapped_positions(state_c6h6)
        )
    ) do (r, img, ru)
        isapprox(r + vecs * SVector(img), ru, atol=1e-10)
    end

    @test all(wrapped_positions(state_c6h6)) do r
        all(vecs \ (r - r0)) do x
            0 <= x < 1
        end
    end
end

@testset "Types not set" begin
    state_c6h6_elt = read_dump("./data/benzene_unwrapped_notype_element.dump")
    state_c6h6_mass = read_dump("./data/benzene_unwrapped_notype_mass.dump")

    type_elt = type_tags(state_c6h6_elt)
    type_mass = type_tags(state_c6h6_mass)
    @test all(eachindex(type_elt, type_mass)) do i
        if mod1(i, 12) <= 6
            type_elt[i] == 1 && type_mass[i] == 2
        else
            type_elt[i] == 2 && type_mass[i] == 1
        end
    end
end

@testset "Triclinic box" begin
    state_c6h6 = read_dump("./data/benzene_wrapped_triclinic.dump")

    @test boxtype(state_c6h6) == :triclinic
end
