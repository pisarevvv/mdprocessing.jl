@testset "Disjoint sets" begin
    d = read_dump("./data/C12H26.clay.100000.dump")
    d = filter(part->part.type > 5, d)

    pairs = neighbor_pairs(d, 1.7)
    ds = MDProcessing.DisjointSets(natoms(d), pairs)
    @test ds.count == natoms(d) / 38
end
