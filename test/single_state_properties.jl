@testset "Radial distribution functions" begin
    system = read_dump("./data/lj.T1.d0.6.0.dump")

    @test rdf(system; rmax=boxvectors(system)[1,1] / 3, nbins=50) isa MDProcessing.RDFMatrix

    @test_throws ArgumentError rdf(system; rmax=boxvectors(system)[1,1] / 1.5, nbins=50)

    @test begin
        rdf(system; rmax=boxvectors(system)[1,1] / 3, nbins=50) ≈
        rdf(system; rmax=boxvectors(system)[1,1] / 3, nbins=50, types=(1,))
    end

    @test begin
        dpe_system = read_dump("data/1,1-DPE.00000")
        rdf(dpe_system; rmax=1.7, nbins=50) ≈
        intra_rdf(dpe_system; rmax=1.7, nbins=50)
    end

    @test begin
        mnp_system = read_dump("data/323.0.995.0.dump")
        molsystem = molecule_planes(mnp_system)

        corr = property_distance_correlation(molsystem; rmax=9.0, nbins=100, propertyname=:normal)
        corr isa AbstractMatrix
    end
end

@testset "Shape parameters" begin
    # Carbons only
    c5_system = filter(p->p[:type] in 14:18, read_dump("./data/C5H12.clay.1000.dump"))

    @test begin
        mol_lines = MDProcessing.molecule_lines(c5_system)
        mols = molecules(c5_system)
        dir = mol_lines[:direction]
        all(zip(dir, mols)) do (dirvec, mol)
            r0, rest = Iterators.peel(position_offsets(mol))
            all(rest) do r
                dot(r - r0, dirvec) >= 0
            end
        end
    end

    system = read_dump("./data/benzene_wrapped.dump")

    benzene_c = filter(p->p[:type]==1, system)

    benzene_mols = molecules(benzene_c)

    benzene1 = benzene_mols[1]

    mol_planes = molecule_planes(benzene_c)

    @test haskey(mol_planes, :normal)

    nrm = mol_planes[:normal]

    @test begin
        all(zip(nrm, benzene_mols)) do (nrmvec, mol)
            all(position_offsets(mol)) do r
                abs(dot(r, nrmvec)) < 1e-8 * norm(r)
            end
        end
    end

    @test begin
        aniso1 = Tuple(anisotropy_factors(benzene1))

        all(benzene_mols) do mol
            aniso = Tuple(anisotropy_factors(mol))
            all(isapprox.(aniso1, aniso))
        end
    end

    @test begin
        rgsq = sum(gyration_tensor(benzene1).values)
        gyration_radius(benzene1) ≈ sqrt(rgsq)
    end

    @test begin
        mols = molecules(benzene_c; weighing=:geometry)
        gyration_radius(mols[1]) ≈ sqrt(sum(gyration_tensor(mols[1]).values))
    end

    @test begin
        shape1 = anisotropy_factors(benzene1)
        aniso1 = shape_anisotropy(benzene1)
        asphere1 = asphericity(benzene1)
        acyl1 = acylindricity(benzene1)

        all(zip(shape1, (asphere1, acyl1, aniso1))) do (factor1, factor2)
            isapprox(factor1, factor2; atol=1e-10)
        end
    end

    @test isapprox(asphericity(benzene1), 0.25; atol=1e-3)

    @test isapprox(acylindricity(benzene1), 0.5; atol=1e-3)

    @test inertia_tensor(benzene1).vectors[:, end:-1:1] ≈ -gyration_tensor(benzene1).vectors
end
