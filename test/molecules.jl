@testset "Molecule interface" begin
    system_mnp = read_dump("./data/323.0.995.0.dump")

    set_typemasses!(system_mnp, [1.008, 12.011, 12.011, 12.011, 1.008])

    @test begin
        mols = molecules(system_mnp)

        parent_state(mols) === system_mnp
    end

    @test begin
        mols = molecules(system_mnp)

        mols.atom_weight === system_mnp._mass
    end

    @test begin
        mols = molecules(system_mnp; weighing="mass")

        mols.atom_weight === system_mnp._mass
    end
    @test begin
        mols = molecules(system_mnp; weighing="geometry")

        mols.atom_weight !== system_mnp._mass
    end
    @test begin
        mols = molecules(system_mnp; weighing=:geometry)

        mols.atom_weight !== system_mnp._mass
    end
    @test begin
        mols = molecules(system_mnp; weighing=:geometry)

        all(==(1), mols.atom_weight)
    end
    @test_throws ArgumentError molecules(system_mnp; weighing=:default)

    @test length(molecules(system_mnp)) == natoms(system_mnp) / 21

    @test begin
        mols = molecules(system_mnp)

        all(pairs(mols)) do (imol, mol)
            collect(atom_indices(mol)) == collect(id_tags(mol)) == imol * 21 - 20:imol * 21
        end
    end

    @test begin
        mols = molecules(system_mnp)
        all(mols) do mol
            natoms(mol) == length(mol) == 21
        end
    end

    @test begin
        mols = molecules(system_mnp)
        all(mols) do mol
            count(p -> mol_tag(p) == mol.id, mol) == 21
        end
    end

    @test begin
        mols = molecules(system_mnp)
        molvel = velocities(mols)
        all(zip(mols, molvel)) do (mol, vel)
            inds = atom_indices(mol)
            vatom = velocities(system_mnp)
            ws = atom_weights(mol)
            vel_ave = sum(w * vatom[ind] for (w, ind) in zip(ws, inds)) / sum(ws)

            isapprox(vel, vel_ave; atol=1e-13)
        end
    end

    @test begin
        mols = molecules(system_mnp)
        molvel = velocities(mols)
        all(zip(mols, molvel)) do (mol, vel)
            isapprox(vel, velocity(mol); atol=1e-13)
        end
    end

    @test begin
        mols = molecules(system_mnp)
        all(mols) do mol
            isapprox(molecule_weight(mol), 11 * 12.011 + 10 * 1.008; atol=1e-14)
        end
    end

    @test begin
        mols = molecules(system_mnp)
        all(pairs(mols)) do (ind, mol)
            id_tag(mol) == mol_tag(mol) == ind
        end
    end

    @test begin
        mols = molecules(system_mnp)
        mws = molecule_weights(mols)
        all(pairs(mols)) do (ind, mol)
            isapprox(molecule_weight(mol), mws[ind]; atol=1e-14)
        end
    end

    @test begin
        mols = molecules(system_mnp)
        all(pairs(mols)) do (ind, mol)
            collect(atom_velocities(mol)) ==
                @view velocities(system_mnp)[ind * 21 - 20 : ind * 21]
        end
    end
end

@testset "Molecule iterators" begin
    system_c6h6 = read_dump("./data/benzene_wrapped_triclinic.dump")

    # Carbons only
    c5_system = filter(p->type_tag(p) in 14:18, read_dump("./data/C5H12.clay.1000.dump"))

    @test begin
        mols = molecules(c5_system)
        all(mols) do mol
            rs = position_offsets(mol)
            dir = fit_line(rs)
            r0, rest = Iterators.peel(rs)
            all(rest) do r
                dot(r - r0, dir) >= 0
            end
        end
    end

    mols_c6h6 = molecules(system_c6h6)

    firstmol_arr = collect(mols_c6h6[1])

    @test all(zip(firstmol_arr, mols_c6h6[1])) do (p1, p2)
        p1 == p2
    end

    @test all(zip(mols_c6h6, id_tags(mols_c6h6), mol_tags(mols_c6h6))) do (mol, idtag, moltag)
        id_tag(mol) == mol_tag(mol) == idtag == moltag
    end

    @test all(mols_c6h6) do mol
        sum(atom_weights(mol)) == molecule_weight(mol)
    end

    @test all(mols_c6h6) do mol
        all(mol) do p
            mol_tag(p) == id_tag(mol) == mol_tag(mol)
        end
    end

    map_com = map(x->(), mols_c6h6)

    @test all(MDProcessing.GLOBAL_DATA_FIELDS) do p
        getfield(map_com, p) == getfield(system_c6h6, p)
    end

    map_natom = map(m->(atom_count=length(m),), mols_c6h6)
    @test haskey(map_natom, :atom_count)
    @test all(==(12), map_natom[:atom_count])

    map_ave_molid = map(mols_c6h6) do mol
        molid = sum(mol_tag, mol) / length(mol)
        return (ave_molid=molid,)
    end

    @test map_ave_molid[:ave_molid] == id_tags(map_ave_molid)
    @test map_ave_molid[:ave_molid] == mol_tags(map_ave_molid)
end

@testset "Printing molecules" begin
    system_c6h6 = read_dump("./data/benzene_wrapped_triclinic.dump")

    mols_c6h6 = molecules(system_c6h6)

    @test string(mols_c6h6) isa String

    @test string(first(mols_c6h6)) isa String
end
