@testset "cluster iterators" begin
    s = read_dump("./data/C12H26.clay.100000.dump")
    s = filter(part->type_tag(part) > 5, s)

    @test_throws DomainError clusters(s; bond_distance=-1.5)

    clust = clusters(s; bond_distance=1.7)
    c1 = clust[1]
    c1_natoms = natoms(c1)

    mols = molecules(s)
    mol1 = mols[1]
    mol1_natoms = natoms(mol1)

    @test mol1_natoms == c1_natoms

    @test length(clust) == natoms(s) / 38

    @test size(clust) == (length(clust),)

    @test all(c->natoms(c)==38, clust)

    @test all(c->length(c)==38, clust)

    s = read_dump("./data/323.0.995.0.dump")
    co_dir = co_directed(s; bond_distance=6.0, cc_thresh=0.95)
    clust_mnp = clusters(s; bond_distance=1.7, sort=true)

    @test natoms(clust_mnp[1]) > 1
    @test length(co_dir) < 512
    m2 = molecules(s, weighing=:geometry)
    mol1 = m2[1]
    inds = collect(atom_indices(mol1))
    a1 = inds[2]
    a2 = inds[1]
    pair = MDProcessing.ParticlePair(s, a1, a2)
    @test same_cluster(pair, clust_mnp)

    @test issorted(co_dir; by=natoms, rev=true)

    @test all(co_dir.state) do part
        part_clust = get_cluster(co_dir, part)
        part in part_clust
    end

    @test all(enumerate(co_dir)) do (iclust, clust)
        all(clust) do part
            co_dir.id[part.idx] == clust.id == iclust
        end
    end

    @test begin
        mol_state = parent_state(co_dir)
        function coplanar(mol_pair)
            local m1, m2 = mol_pair
            nrm = mol_state[:normal]
            return abs(cc(nrm[m1], nrm[m2])) > 0.95
        end
        add_clusters!(mol_state; bond_distance=6.0, predicate=coplanar)
        mol_state[:cluster_id] == co_dir.id
    end
end
