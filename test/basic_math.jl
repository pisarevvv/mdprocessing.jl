@testset "Periodic boundaries" begin
    v1 = [0.5, 0.5, 0.5]
    v2 = [sqrt(2), sqrt(2), sqrt(2)]

    sv1 = SVector{3}(v1)
    sv2 = SVector{3}(v2)

    box1 = (1.0, 1.0, 1.0)
    box2 = (2.0, 2.0, 2.0)

    @test pbcvector(sv1, sv2, box1, (false, false, false)) == sv2 - sv1

    @test pbcvector(sv1, sv2, box1, (true, true, true)) == (sv2 - sv1) .- box1

    @test pbcvector(sv1, sv2, SVector(box1)) == (sv2 - sv1) .- box1

    @test pbcdist(sv1, sv2, SVector(box1)) == norm((sv2 - sv1) .- box1)

    @test pbcdist2(sv1, sv2, SVector(box1)) ≈ norm((sv2 - sv1) .- box1)^2

    @test pbcdist(v1, v2, box1) == norm((sv2 - sv1) .- box1)

    @test_throws DimensionMismatch pbcdist([0.5, 0.5], [0.2, 0.0], (2.0, 2.0))

    @test_throws DimensionMismatch pbcdist(v1, [0.2], box1)

    system = read_dump("data/lj.T1.d0.6.0.dump")
    sysbox = diag(boxvectors(system))
    coord = wrapped_positions(system)

    ppair = MDProcessing.ParticlePair(system, 1, 500)

    @test pbcvector(coord[1], coord[500], sysbox, system.pbc) == pbcvector(ppair)
    @test pbcdist(coord[1], coord[500], sysbox) == pbcdist(ppair)
end

@testset "Miscellaneous maths" begin

    system = read_dump("data/lj.T1.d0.6.0.dump")
    sysbox = diag(boxvectors(system))
    coord = wrapped_positions(system)

    @test let
        nrmr1 = normalize(coord[1])
        nrmr500 = normalize(coord[500])

        MDProcessing.cc(nrmr1, nrmr500) ≈ nrmr1' * nrmr500
    end

    @test let
        nrmr1 = normalize(coord[1])
        nrmr500 = normalize(coord[500])

        MDProcessing.cc(nrmr1, nrmr500) ≈ MDProcessing.cc(Vector(coord[1]), coord[500])
    end

    @test isapprox(last(MDProcessing.integrate_trap(1 ./ (1:0.01:10), 0.01)), log(10); rtol=1e-4)
end
