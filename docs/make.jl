using Documenter, MDProcessing

#DocMeta.setdocmeta!(MDProcessing, :DocTestSetup, :(using MDProcessing); recursive=true)

makedocs(;
    modules = [MDProcessing, MDProcessing.Autocorr],
    checkdocs = :exports,
    sitename = "MDProcessing.jl documentation",
    format = Documenter.HTML(
        prettyurls = get(ENV, "CI", nothing) == "true"
    ),
    repo = Remotes.GitLab("pisarevvv", "mdprocessing.jl"),
    pages = [
        "Home" => "index.md",
        "Release Notes" => "release_notes.md"
    ],
    warnonly = true,
)

deploydocs(;
  repo = "gitlab.com/pisarevvv/mdprocessing.jl",
  devurl = "dev",
  devbranch = "dev",
  versions = ["stable" => "v^", "v#.#.#", "dev" => "dev"],
)
