# 0.5

* Improved performance of neighbor search
* Added a new function `neighbor_pairs` to return an iterator over all pairs of neighbors within a given cut-off radius
* Added `molecules(system; weighing=[:geometry | :mass])` function which returns an iterator over all molecules
* Wrapped and unwrapped coordinates can now be accessed via `wrapped_positions(state)` and `unwrapped_positions(state)`
* Reading dumps with element string is now supported
* The type of simulation cell can be obtained as `boxtype(state)` and can be `:orthogonal` or `:triclinic`
* More interface functions: `iswrapped`, `hasvelocity`, `has_element_name`, `hasmass`
* Setters for mass based on type or element information
* Computation of shape parameters for molecules
* Data structure and algorithms to compute clusters in system based on maximum separation or from a list of connections

### Breaking changes
* cell list is moved from `MDState` properties to the `NeighborPairs` data structure. Low-level cell list operations like `build_cell_list!` are no more exported, the users are encouraged to work with neighbors using `neighbor_pairs` function
* `state.coords` now stores the wrapped coordinates, users are encouraged to use `wrapped_positions(state)` and `unwrapped_positions(state)` to access the particle coordinates
* atom fields are accessed by indexing (e.g., `state[:mass]`) rather than by properties
* Interface of `traj_average` changed to `traj_average(fn, fmask; timesteps)`
* Interface of `traj_correlate` changed to `traj_correlate(fn, fmask; initial_steps, step_delta)`