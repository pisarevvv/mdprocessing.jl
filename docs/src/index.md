# MDProcessing.jl documentation

## Reading LAMMPS output files

```@docs
read_dump

read_dump!
```

## Type constructors
```@docs
MDState
```

## Basic interface

### Getting state properties
```@docs
timestep

natoms(::MDState)

boxtype

boxvectors

boxorigin

pbcflags

volume

hasmass

masses

iswrapped

wrapped_positions

unwrapped_positions

hasvelocity

velocities

has_element_name

element_names

id_tags

mol_tags

type_tags
```

### Setting masses and element names
```@docs
set_typenames!

setmasses!

set_typemasses!

set_elementmasses!
```

## Atoms
```@docs
MDProcessing.MDParticle

wrapped_position(p::MDProcessing.MDParticle)

unwrapped_position(p::MDProcessing.MDParticle)

velocity(p::MDProcessing.MDParticle)

atom_index

id_tag(p::MDProcessing.MDParticle)

mol_tag(p::MDProcessing.MDParticle)

type_tag(::MDProcessing.MDParticle)
```
### Filter particles by some property
```@docs
Base.filter(fn, ::MDState)
```

## Molecules

```@docs
molecules

parent_state(::MDProcessing.Molecules)

molecule_weights

natoms(::MDProcessing.Molecule)

id_tag(::MDProcessing.Molecule)

mol_tag(::MDProcessing.Molecule)

velocity(::MDProcessing.Molecule)

atom_weights

atom_indices

position_offsets

atom_velocities

wrapped_position(mol::MDProcessing.Molecule)

unwrapped_position(mol::MDProcessing.Molecule)

Base.map(fn, mol::MDProcessing.Molecules)
```

### Shape parameters

```@docs
fit_normal

fit_line

molecule_planes

molecule_lines

gyration_radius

gyration_tensor

inertia_tensor

asphericity

acylindricity

shape_anisotropy

anisotropy_factors
```

## Neighbor search

```@docs
neighbor_pairs

MDProcessing.ParticlePair

atom_indices(p::MDProcessing.ParticlePair)

pbcvector

pbcdist

pbcdist2
```

## Clusters
```@docs
MDProcessing.Clusters

MDProcessing.Cluster

clusters

parent_state(::MDProcessing.Clusters)

add_clusters!

same_cluster

atom_indices(::MDProcessing.Cluster)

natoms(::MDProcessing.Cluster)

get_cluster
```

## Trajectory functions

```@docs
traj_average

traj_correlate
```

## Adding a property

```@docs
addproperty!
```

## Autocorrelation

The functions below are part of `MDProcessing.Autocorr` module.

```@docs
Autocorr.acf_fft

Autocorr.integrate
```

## Other analysis functions

```@docs
center_of_mass

displacements

msd

rdf

rdf!

intra_rdf

distance_range

type_pairs
```
